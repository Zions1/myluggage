package test;

import myLuggage.configuration.WebMvcConfiguration;
import myLuggage.persist.dtos.ActivityDTO;
import myLuggage.persist.dtos.SeasonDTO;
import myLuggage.services.IActivityService;
import myLuggage.services.ISeasonService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebMvcConfiguration.class})
@WebAppConfiguration
@Transactional
public class ActivityIntegrationTest {

    @Autowired
    private IActivityService activityService;

    @Autowired
    private ISeasonService seasonService;

    @Test
    public void testActivity(){
        ActivityDTO testActivity = new ActivityDTO();
        testActivity.setName("aaa");
        activityService.addActivity(testActivity);

        assert testActivity == activityService.getActivityById(1000);
    }

    @Test
    public void testSeason(){
        SeasonDTO seasonDTO = new SeasonDTO();
        seasonDTO.setId(100);
        seasonDTO.setName("aaa");
        seasonService.addSeason(seasonDTO);
        assert true;
    }




}