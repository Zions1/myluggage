package myLuggage.controller;

import myLuggage.persist.dtos.ActivityDTO;
import myLuggage.services.IActivityService;
import myLuggage.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class ActivityController {

    @Autowired
    private IUserService userService;

    @Autowired
    private IActivityService activityService;

    @RequestMapping(value = {"/activities"}, method = RequestMethod.GET)
    public String activity(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("activities", activityService.getAllActivities());
        return "/activity/activities";
    }

    @RequestMapping(value = {"/addActivity"}, method = RequestMethod.GET)
    public String addActivity(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("activity", new ActivityDTO());
        return "/activity/addActivity";
    }

    @RequestMapping(value = {"/addActivity"}, method = RequestMethod.POST)
    public String addActivity(Model model, @ModelAttribute(value = "activity") ActivityDTO activity, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError", true);
            return "/addActivity";
        }
        activityService.addActivity(activity);
        return "redirect:/activities";
    }

    @RequestMapping(value = {"activities/{id}/deleteActivity"}, method = RequestMethod.GET)
    public String deleteActivity(@PathVariable(value = "id") Integer id) {
        activityService.deleteActivity(id);
        return "redirect:/activities";
    }

    @RequestMapping(value = {"activities/{id}/editActivity"}, method = RequestMethod.GET)
    public String editActivity(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("activity", activityService.getActivityById(id));
        return "/activity/editActivity";
    }

    @RequestMapping(value = {"activities/{id}/editActivity"}, method = RequestMethod.POST)
    public String editActivity(Model model, @ModelAttribute(value = "activity") ActivityDTO activity, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError", true);
            return "/editActivity";
        }
        activityService.updateActivity(activity);
        return "redirect:/activities";
    }


}
