package myLuggage.controller;

import myLuggage.persist.dtos.TransportDTO;
import myLuggage.services.ITransportService;
import myLuggage.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class TransportController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ITransportService transportService;

    @RequestMapping(value = {"/transports"}, method = RequestMethod.GET)
    public String transport(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("transports", transportService.getAllTransports());
        return "/transport/transports";
    }

    @RequestMapping(value = {"/addTransport"}, method = RequestMethod.GET)
    public String addTransport(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("transport", new TransportDTO());
        return "/transport/addTransport";
    }

    @RequestMapping(value = {"/addTransport"}, method = RequestMethod.POST)
    public String addTransport(Model model, @ModelAttribute(value = "transport") TransportDTO transport, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError",true);
            return "redirect:/addTransport";
        }
        transportService.addTransport(transport);
        return "redirect:/transports";
    }

    @RequestMapping(value = {"transports/{id}/deleteTransport"}, method = RequestMethod.GET)
    public String deleteTransport(@PathVariable(value = "id") Integer id) {
        transportService.deleteTransport(id);
        return "redirect:/transports";
    }

    @RequestMapping(value = {"transports/{id}/editTransport"}, method = RequestMethod.GET)
    public String editTransport(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("transport", transportService.getTransportById(id));
        return "/transport/editTransport";
    }

    @RequestMapping(value = {"transports/{id}/editTransport"}, method = RequestMethod.POST)
    public String editTransport(Model model, @ModelAttribute(value = "transport") TransportDTO transportDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError",true);
            return "redirect:/editTransport";
        }
        transportService.updateTransport(transportDTO);
        return "redirect:/transports";
    }



}
