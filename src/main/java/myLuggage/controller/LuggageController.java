package myLuggage.controller;

import myLuggage.persist.dtos.LuggageDTO;
import myLuggage.persist.dtos.LuggagesThingsDTO;
import myLuggage.persist.dtos.TravelDTO;
import myLuggage.persist.model.*;
import myLuggage.report.excel.LuggageExcelReportView;
import myLuggage.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.beans.PropertyEditorSupport;

@Controller
public class LuggageController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ITravelService travelService;

    @Autowired
    private IThingService thingService;

    @Autowired
    private ILuggageService luggageService;

    @Autowired
    private ILuggageTypeService luggageTypeService;

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private ISeasonService seasonService;

    @Autowired
    private IActivityService activityService;


    @RequestMapping(value = {"/myLuggages"}, method = RequestMethod.GET)
    public String myLuggages(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("luggages", luggageService.getAllUserLuggages(UserController.getPrincipal()));
        return "luggage/myLuggages";
    }


    @RequestMapping(value = {"myTravels/{id}/addLuggage"}, method = RequestMethod.GET)
    public String addLuggage(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("travel", travelService.getTravelById(id));
        model.addAttribute("types", luggageTypeService.getAllLuggageTypes());
        model.addAttribute("categories", categoryService.getAllCategories());
        model.addAttribute("luggage", new LuggageDTO());
        return "luggage/addLuggage";
    }

    @RequestMapping(value = {"myTravels/{travelId}/addLuggage"}, method = RequestMethod.POST)
    public String addLuggage(Model model, @PathVariable(value = "travelId") Integer travelId, @ModelAttribute(value = "luggage") LuggageDTO luggage, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError", true);
            return "redirect:/myTravels/" + travelId + "/addLuggage";
        }
        luggage.setTravel(travelService.getTravelEntityById(travelId));
        luggageService.addLuggage(luggage);
        int luggageId = luggageService.getAllLuggages().get(luggageService.getAllLuggages().size() - 1).getId();
        return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
    }

    @RequestMapping(value = {"myLuggages/{luggageId}/luggageThingsCreator"}, method = RequestMethod.GET)
    public String luggageThingsCreator(Model model, @PathVariable(value = "luggageId") Integer luggageId) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        LuggageDTO luggage = luggageService.getLuggageById(luggageId);
        TravelDTO travel = travelService.getTravelById(luggage.getTravel().getId());

        model.addAttribute("luggage", luggage);
        model.addAttribute("travel", travel);
        model.addAttribute("things", thingService.getThingsWithoutSelected(luggage.getThings()));
        model.addAttribute("travelActivities", travel.getActivities());

        model.addAttribute("activities", activityService.getAllActivities());
        model.addAttribute("categories", categoryService.getAllCategories());
        model.addAttribute("types", luggageTypeService.getAllLuggageTypes());
        model.addAttribute("seasons", seasonService.getAllSeasons());

        model.addAttribute("luggageThing", new LuggagesThingsDTO());
        return "luggage/luggageThingsCreator";
    }

    @RequestMapping(value = {"myLuggages/{luggageId}/updateLuggageName"}, method = RequestMethod.POST)
    public String updateLuggageName(Model model, @PathVariable(value = "luggageId") Integer luggageId,
                                    @ModelAttribute(value = "luggageName") String name, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError", true);
            return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
        }
        luggageService.updateLuggageName(luggageId, name);
        return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
    }

    @RequestMapping(value = {"myLuggages/{id}/addLuggageThings"}, method = RequestMethod.POST)
    public String addLuggageThings(Model model, @PathVariable(value = "id") Integer luggageId,
                                   @ModelAttribute(value = "luggage") LuggageDTO luggage, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError", true);
            return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
        }
        luggageService.addLuggageThings(luggage);
        return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
    }


    @RequestMapping(value = {"myLuggages/{id}/addCustomThing"}, method = RequestMethod.POST)
    public String addCustomThing(Model model, @PathVariable(value = "id") Integer luggageId,
                                 @ModelAttribute(value = "luggageThing") LuggagesThingsDTO luggageThing, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
        }
        if (!thingService.thingExist(luggageThing.getThing())) {
            thingService.addThingEntity(luggageThing.getThing());
        }
        luggageService.updateLuggageThing(luggageThing, luggageId);
        return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
    }

    @RequestMapping(value = {"myLuggages/{id}/editLuggage"}, method = RequestMethod.POST)
    public String editLuggage(Model model, @PathVariable(value = "id") Integer luggageId, @ModelAttribute(value = "luggage") LuggageDTO luggage, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
        }
        luggageService.addLuggageThings(luggage);
        return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
    }

//    @RequestMapping(value = {"myLuggages/{id}/editLuggage"}, method = RequestMethod.GET)
//    public String editLuggage(Model model, @PathVariable(value = "id") Integer luggageId) {
//        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
//
//        LuggageDTO luggage = luggageService.getLuggageById(luggageId);
//        TravelDTO travel = travelService.getTravelById(luggage.getTravel().getId());
//        model.addAttribute("luggage", luggage);
//        model.addAttribute("travel", travel);
//
//        model.addAttribute("categories", categoryService.getAllCategories());
//        model.addAttribute("seasons", seasonService.getAllSeasons());
//        model.addAttribute("activities", travel.getActivities());
//        // model.addAttribute("activities", activityService.getAllActivities());
//        model.addAttribute("things", thingService.getThingsWithoutSelected(luggage.getThings()));
//
//
//        model.addAttribute("luggageThing", new LuggagesThingsDTO());
//        return "luggage/editLuggage";
//    }


    @RequestMapping(value = {"myLuggages/{id}/updateLuggage"}, method = RequestMethod.POST)
    public String updateLuggage(Model model, @PathVariable(value = "id") Integer luggageId, @ModelAttribute(value = "luggage") LuggageDTO luggage, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
        }
        luggageService.updateLuggage(luggage);
        return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
    }

    @RequestMapping(value = {"myLuggages/{id}/deleteLuggage"}, method = RequestMethod.GET)
    public String deleteLuggage(Model model, @PathVariable(value = "id") Integer luggageId) {
        luggageService.deleteLuggage(luggageId);
        return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
    }

    @RequestMapping(value = {"myLuggages/{luggageId}/deleteLuggageThing/{thingId}"}, method = RequestMethod.GET)
    public String deleteLuggageThing(Model model, @PathVariable(value = "luggageId") Integer luggageId, @PathVariable(value = "thingId") Integer thingId) {
        luggageService.deleteLuggageThing(luggageId, thingId);
        return "redirect:/myLuggages/" + luggageId + "/luggageThingsCreator";
    }

    @RequestMapping(value = {"myLuggages/{id}"}, method = RequestMethod.GET)
    public String luggageDetails(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("luggage", luggageService.getLuggageById(id));
        return "luggage/luggageDetails";
    }


    @RequestMapping(value = {"myLuggages/{luggageId}/generateLuggageExcelReport"}, method = RequestMethod.GET)
    public ModelAndView generateLuggageExcelReport(Model model, @PathVariable(value = "luggageId") Integer luggageId) {
        model.addAttribute("travel", travelService.getTravelById(luggageService.getLuggageById(luggageId).getTravel().getId()));
        return new ModelAndView(new LuggageExcelReportView(), "luggage", luggageService.getLuggageById(luggageId));
    }


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(LuggagesThingsEntity.class, "things", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                    String thingId = text.substring(text.indexOf("=") + 1, text.indexOf(','));
                    String quantity = text.substring(text.lastIndexOf("=") + 1, text.length());
                    LuggagesThingsEntity luggagesThings = new LuggagesThingsEntity();
                    luggagesThings.setThing(thingService.getThingEntityById(Integer.parseInt(thingId)));
                    luggagesThings.setQuantity(Integer.parseInt(quantity));
                    setValue(luggagesThings);
            }
        });
        binder.registerCustomEditor(LuggageTypeEntity.class, "type", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(luggageTypeService.getLuggageTypeEntityByName(text));
            }
        });
        binder.registerCustomEditor(CategoryEntity.class, "category", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(categoryService.getCategoryEntityByName(text));
            }
        });
        binder.registerCustomEditor(TravelEntity.class, "travel", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(travelService.getTravelEntityById(Integer.parseInt(text)));
            }
        });
        binder.registerCustomEditor(CategoryEntity.class, "thing.category", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(categoryService.getCategoryEntityByName(text));
            }
        });

        binder.registerCustomEditor(SeasonEntity.class, "thing.season", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(seasonService.getSeasonEntityByName(text));
            }
        });
        binder.registerCustomEditor(ActivityEntity.class, "thing.activity", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(activityService.getActivityEntityByName(text));
            }
        });
    }

}
