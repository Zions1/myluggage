package myLuggage.controller;

import myLuggage.persist.dtos.PlaceDTO;
import myLuggage.persist.dtos.TravelDTO;
import myLuggage.persist.model.*;
import myLuggage.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 */
@Controller
public class TravelController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ITravelService travelService;

    @Autowired
    private ICountryService countryService;

    @Autowired
    private ISeasonService seasonService;

    @Autowired
    private ITransportService transportService;

    @Autowired
    private IDiningOptionService diningOptionService;

    @Autowired
    private IActivityService activityService;

    @Autowired
    private IAccommodationService accommodationService;

    @Autowired
    private IPlaceService placeService;

    @RequestMapping(value = {"/myTravels"}, method = RequestMethod.GET)
    public String myTravels(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("travels", travelService.getAllUserTravels((UserController.getPrincipal())));
        return "travel/myTravels";
    }

    @RequestMapping(value = {"/addTravel"}, method = RequestMethod.GET)
    public String addTravel(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("countries", countryService.getAllCountries());
        model.addAttribute("diningOptions", diningOptionService.getAllDiningOptions());
        model.addAttribute("transports", transportService.getAllTransports());
        model.addAttribute("accommodations", accommodationService.getAllAccommodations());
        model.addAttribute("activities", activityService.getAllActivitiesWithoutBasic());
        model.addAttribute("travel", new TravelDTO());
        return "travel/addTravel";
    }

    @RequestMapping(value = {"/addTravel"}, method = RequestMethod.POST)
    public String addTravel(Model model, @ModelAttribute(value = "travel") TravelDTO travelDTO,
                            BindingResult bindingResult, HttpServletRequest request)  {
        if (bindingResult.hasErrors()) {
            return "redirect:/addTravel";
        }
        int id = travelService.addTravel(travelDTO);
        return UserController.getPrincipal().equals("") ? "redirect:/myTravels/"+ id +"/addLuggage" : "redirect:/myTravels";
    }


    @RequestMapping(value = {"myTravels/{id}/deleteTravel"}, method = RequestMethod.GET)
    public String deleteTravel(Model model, @PathVariable(value = "id") Integer id) {
        travelService.deleteTravel(id);
        return "redirect:/myTravels";
    }

    @RequestMapping(value = {"myTravels/{id}/editTravel"}, method = RequestMethod.GET)
    public String editTravel(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        TravelDTO travel = travelService.getTravelById(id);
        model.addAttribute("travel", travel);
        model.addAttribute("countries", countryService.getAllCountries());
        model.addAttribute("diningOptions", diningOptionService.getAllDiningOptions());
        model.addAttribute("transports", transportService.getTransportsWithoutSelected(travel.getTransports()));
        model.addAttribute("accommodations", accommodationService.getAllAccommodations());
        model.addAttribute("activities", activityService.getActivitiesWithoutSelected(travel.getActivities()));
        model.addAttribute("seasons", seasonService.getAllSeasons());
        return "travel/editTravel";
    }

    @RequestMapping(value = {"myTravels/{id}/editTravel"}, method = RequestMethod.POST)
    public String editTravel(Model model, @PathVariable(value = "id") Integer id, @ModelAttribute(value = "travel") TravelDTO travel, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "redirect:/myTravels/" + id + "/editTravel";
        }
        travelService.updateTravel(travel);
        model.addAttribute("travel", travel);
        return "redirect:/myTravels";
    }


    @RequestMapping(value = {"myTravels/{id}"}, method = RequestMethod.GET)
    public String travelDetails(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("travel", travelService.getTravelById(id));
        return "travel/travelDetails";
    }


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));

        binder.registerCustomEditor(UserEntity.class, "user", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(userService.getUserEntityByEmail((UserController.getPrincipal())));
            }
        });
        binder.registerCustomEditor(TransportEntity.class, "transports", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(transportService.getTransportEntityById(Integer.parseInt(text)));
            }
        });
        binder.registerCustomEditor(AccommodationEntity.class, "accommodation", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(accommodationService.getAccommodationEntityById(Integer.parseInt(text)));
            }
        });
        binder.registerCustomEditor(DiningOptionEntity.class, "diningOption", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(diningOptionService.getDiningOptionEntityById(Integer.parseInt(text)));
            }
        });
        binder.registerCustomEditor(CountryEntity.class, "country", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(countryService.getCountryEntityById(Integer.parseInt(text)));
            }
        });
        binder.registerCustomEditor(SeasonEntity.class, "season", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(seasonService.getSeasonEntityByName(text));
            }
        });
        binder.registerCustomEditor(ActivityEntity.class, "activities", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(activityService.getActivityEntityById(Integer.parseInt(text)));
            }
        });

        binder.registerCustomEditor(PlaceEntity.class, "places", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
//                String city = text.substring(0, text.indexOf(','));
//                String country = text.substring(text.indexOf(' ')+1, text.length());
//                PlaceEntity place = travelService.getPlaceEntityByName(city);
//                if (place != null) {
//                    setValue(place);
//                } else {
//                    PlaceDTO travelPlaceDTO = new PlaceDTO(travelService.getCountryEntityByName(country), city);
//                    travelService.addPlace(travelPlaceDTO);
//                    setValue(travelService.getPlaceEntityByName(city));
//                }

                PlaceEntity place = placeService.getPlaceEntityByName(text);
                if (place != null) {
                    setValue(place);
                } else {
                    PlaceDTO placeDTO = new PlaceDTO(text);
                    placeService.addPlace(placeDTO);
                    setValue(placeService.getPlaceEntityByName(text));
                }
            }
        });
    }

}
