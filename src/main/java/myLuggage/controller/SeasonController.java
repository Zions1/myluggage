package myLuggage.controller;

import myLuggage.persist.dtos.SeasonDTO;
import myLuggage.services.ISeasonService;
import myLuggage.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class SeasonController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ISeasonService seasonService;

    @RequestMapping(value = {"/seasons"}, method = RequestMethod.GET)
    public String season(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("seasons", seasonService.getAllSeasons());
        return "/season/seasons";
    }

    @RequestMapping(value = {"/addSeason"}, method = RequestMethod.GET)
    public String addSeason(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("season", new SeasonDTO());
        return "/season/addSeason";
    }

    @RequestMapping(value = {"/addSeason"}, method = RequestMethod.POST)
    public String addSeason(Model model, @ModelAttribute(value = "season") SeasonDTO season, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError",true);
            return "redirect:/addSeason";
        }
        seasonService.addSeason(season);
        return "redirect:/seasons";
    }

    @RequestMapping(value = {"seasons/{id}/deleteSeason"}, method = RequestMethod.GET)
    public String deleteSeason(@PathVariable(value = "id") Integer id) {
        seasonService.deleteSeason(id);
        return "redirect:/seasons";
    }

    @RequestMapping(value = {"seasons/{id}/editSeason"}, method = RequestMethod.GET)
    public String editSeason(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("season", seasonService.getSeasonById(id));
        return "/season/editSeason";
    }

    @RequestMapping(value = {"seasons/{id}/editSeason"}, method = RequestMethod.POST)
    public String editSeason(Model model, @ModelAttribute(value = "season") SeasonDTO seasonDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError",true);
            return "redirect:/editSeason";
        }
        seasonService.updateSeason(seasonDTO);
        return "redirect:/seasons";
    }



}
