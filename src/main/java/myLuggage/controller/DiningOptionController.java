package myLuggage.controller;

import myLuggage.persist.dtos.DiningOptionDTO;
import myLuggage.services.IDiningOptionService;
import myLuggage.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class DiningOptionController {

    @Autowired
    private IUserService userService;

    @Autowired
    private IDiningOptionService diningOptionService;

    @RequestMapping(value = {"/diningOptions"}, method = RequestMethod.GET)
    public String diningOption(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("diningOptions", diningOptionService.getAllDiningOptions());
        return "/diningOption/diningOptions";
    }

    @RequestMapping(value = {"/addDiningOption"}, method = RequestMethod.GET)
    public String addDiningOption(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("diningOption", new DiningOptionDTO());
        return "/diningOption/addDiningOption";
    }

    @RequestMapping(value = {"/addDiningOption"}, method = RequestMethod.POST)
    public String addDiningOption(Model model, @ModelAttribute(value = "diningOption") DiningOptionDTO diningOption, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError",true);
            return "redirect:/addDiningOption";
        }
        diningOptionService.addDiningOption(diningOption);
        return "redirect:/diningOptions";
    }

    @RequestMapping(value = {"diningOptions/{id}/deleteDiningOption"}, method = RequestMethod.GET)
    public String deleteDiningOption(@PathVariable(value = "id") Integer id) {
        diningOptionService.deleteDiningOption(id);
        return "redirect:/diningOptions";
    }

    @RequestMapping(value = {"diningOptions/{id}/editDiningOption"}, method = RequestMethod.GET)
    public String editDiningOption(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("diningOption", diningOptionService.getDiningOptionById(id));
        return "/diningOption/editDiningOption";
    }

    @RequestMapping(value = {"diningOptions/{id}/editDiningOption"}, method = RequestMethod.POST)
    public String editDiningOption(Model model, @ModelAttribute(value = "diningOption") DiningOptionDTO diningOption, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError",true);
            return "redirect:/editDiningOption";
        }
        diningOptionService.updateDiningOption(diningOption);
        return "redirect:/diningOptions";
    }



}
