package myLuggage.controller;

import myLuggage.persist.dtos.LuggageTypeDTO;
import myLuggage.services.ILuggageTypeService;
import myLuggage.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class LuggageTypeController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ILuggageTypeService luggageTypeService;

    @RequestMapping(value = {"/luggageTypes"}, method = RequestMethod.GET)
    public String luggageType(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("luggageTypes", luggageTypeService.getAllLuggageTypes());
        return "/luggageType/luggageTypes";
    }

    @RequestMapping(value = {"/addLuggageType"}, method = RequestMethod.GET)
    public String addLuggageType(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("luggageType", new LuggageTypeDTO());
        return "/luggageType/addLuggageType";
    }

    @RequestMapping(value = {"/addLuggageType"}, method = RequestMethod.POST)
    public String addLuggageType(Model model, @ModelAttribute(value = "luggageType") LuggageTypeDTO luggageType, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError",true);
            return "redirect:/addLuggageType";
        }
        luggageTypeService.addLuggageType(luggageType);
        return "redirect:/luggageTypes";
    }

    @RequestMapping(value = {"luggageTypes/{id}/deleteLuggageType"}, method = RequestMethod.GET)
    public String deleteLuggageType(@PathVariable(value = "id") Integer id) {
        luggageTypeService.deleteLuggageType(id);
        return "redirect:/luggageTypes";
    }

    @RequestMapping(value = {"luggageTypes/{id}/editLuggageType"}, method = RequestMethod.GET)
    public String editLuggageType(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("luggageType", luggageTypeService.getLuggageTypeById(id));
        return "/luggageType/editLuggageType";
    }

    @RequestMapping(value = {"luggageTypes/{id}/editLuggageType"}, method = RequestMethod.POST)
    public String editLuggageType(Model model, @ModelAttribute(value = "luggageType") LuggageTypeDTO luggageType, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError",true);
            return "redirect:/editLuggageType";
        }
        luggageTypeService.updateLuggageType(luggageType);
        return "redirect:/luggageTypes";
    }



}
