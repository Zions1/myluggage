package myLuggage.controller;

import com.google.common.io.ByteSource;
import com.google.common.io.ByteStreams;
import myLuggage.persist.dtos.UserAvatarDTO;
import myLuggage.persist.dtos.UserRoleDTO;
import myLuggage.persist.model.TravelEntity;
import myLuggage.persist.model.UserAvatarEntity;
import myLuggage.persist.model.UserRoleEntity;
import myLuggage.services.IEmailService;
import myLuggage.services.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import myLuggage.persist.dtos.UserDTO;
import myLuggage.services.IUserService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialBlob;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.io.*;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Provides controller for user.
 *
 * @author Wiktor Florencki
 */
@Controller
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    protected IEmailService emailService;

    @Autowired
    protected IUserRoleService roleService;

    //Start: Registration

    /**
     * Registration page.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = {"/registration"}, method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("user", new UserDTO());
        return "registration";
    }

    /**
     * Creates new user.
     *
     * @param model
     * @param userDTO
     * @param bindingResult
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = {"/registration"}, method = RequestMethod.POST)
    public String registration(Model model, @Valid @ModelAttribute(value = "user") UserDTO userDTO, BindingResult bindingResult) throws SQLException {
        if (bindingResult.hasErrors()) {
            return "redirect:/registration";
        }
        if (userDTO != null &&
                userService.getUserByUsername(userDTO.getUsername()) == null &&
                userService.getUserByEmail(userDTO.getEmail()) == null) {
            userService.addUser(userDTO);
            emailService.confirmRegistration(userService.getUserByEmail(userDTO.getEmail()));
            return "redirect:/login";
        }
        model.addAttribute("isBusyUsernameOrEmailError", true);
        return "/registration";
    }

    /**
     * Confirm registration page.
     *
     * @param token
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = {"/confirmRegistration"}, method = RequestMethod.GET)
    public String confirmRegistration(@RequestParam(value = "token") String token) throws SQLException {
        List<UserDTO> userList = userService.getAllUsers();
        for (UserDTO user : userList) {
            if (user.getToken() != null && user.getToken().equals(token)) {
                userService.activateUser(user);
                return "login";
            }
        }
        return "error";
    }
    //End: Registration

    //Begin: Login

    /**
     * Login page.
     *
     * @return
     */
    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    /**
     * Allows logout a user.
     *
     * @param request
     * @param response
     * @return
     */
    @PreAuthorize("!hasRole('ROLE_USER')")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }
    //End: Login

    //Begin: Forgot password

    /**
     * Forgot password page.
     *
     * @return forgot password page
     */
    @RequestMapping(value = {"/forgotPassword"}, method = RequestMethod.GET)
    public String forgotPassword() {
        return "forgotPassword";
    }

    /**
     * Allows change user password and automatic send new random password via email.
     *
     * @param request
     * @return
     * @throws SQLException
     */
    @RequestMapping(value = {"/forgotPassword"}, method = RequestMethod.POST)
    public String forgotPassword(HttpServletRequest request) throws SQLException {
        String email = request.getParameter("email");
        UserDTO user = userService.getUserByEmail(email);
        if (user != null) {
            String newPassword = UUID.randomUUID().toString().replace("-", "").substring(0, 10);
            userService.changePassword(user.getId(), newPassword);
            emailService.resetPassword(user, newPassword);
            return "/login";
        }
        return "/forgotPassword";
    }
    //End: Forgot password

    //Begin: User profile
    /**
     * Get current user.
     *
     * @param model
     * @param request
     * @return
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/editProfile"}, method = RequestMethod.GET)
    public String editProfile(Model model, HttpServletRequest request){
        model.addAttribute("user", userService.getUserByEmail(request.getUserPrincipal().getName()));
        return "/user/editProfile";
    }

    /**
     * Modifies existing user avatar.
     *
     * @param user1
     * @param file
     * @param bindingResult
     * @return
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/editProfileAvatar"}, method = RequestMethod.POST)
    public String editProfileAvatar(@Valid @ModelAttribute(value = "user") UserDTO user1,
                                    @RequestParam(value = "avatarFile") MultipartFile file, BindingResult bindingResult) {
        UserDTO user = userService.getUserByEmail(UserController.getPrincipal());
        if (bindingResult.hasErrors()) {
            return "/editProfile";
        }
        UserAvatarDTO avatar = new UserAvatarDTO();
        if (!file.isEmpty()) {
            String name = file.getName();
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name + "-uploaded")));
                stream.write(bytes);
                stream.close();
                avatar.setAvatar(new SerialBlob(bytes));
            } catch (Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        }
        userService.updateUserAvatar(user, avatar);
        return "redirect:/editProfile";
    }

    /**
     * Modifies existing user details.
     *
     * @param user
     * @param bindingResult
     * @return
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/editProfileDetails"}, method = RequestMethod.POST)
    public String editProfileDetails(@Valid @ModelAttribute(value = "user") UserDTO user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "/editProfile";
        }
        userService.updateUserDetails(user);
        return "redirect:/editProfile";
    }

    /**
     * Allows change user password.
     *
     * @param request
     * @return
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/changePassword"}, method = RequestMethod.POST)
    public String changePassword(HttpServletRequest request){
        UserDTO user = userService.getUserByEmail(request.getUserPrincipal().getName());
        String oldPassword = request.getParameter("oldPassword");
        String newPassword = request.getParameter("newPassword");
        userService.changePassword(user.getId(), oldPassword, newPassword);
        return "redirect:/editProfile";
    }
    //End: User profile

    /**
     * Gets user list.
     * @param model
     * @return
     */
    @RequestMapping(value = {"/users"}, method = RequestMethod.GET)
    public String user(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("users", userService.getAllUsers());
        return "/user/users";
    }

    /**
     * Delete user.
     * @param id
     * @return
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = {"users/{id}/deleteUser"}, method = RequestMethod.GET)
    public String deleteUser(@PathVariable(value = "id") Integer id) {
        userService.deleteUser(id);
        return "redirect:/users";
    }

    /**
     * Modifies a user with administrator privileges page.
     *
     * @param model
     * @param id
     * @return
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = {"users/{id}/editUser"}, method = RequestMethod.GET)
    public String editUser(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("userEdit", userService.getUserById(id));
        model.addAttribute("roles", roleService.getAllRoles());
        return "/user/editUser";
    }

    /**
     * Editing a user with administrator privileges.
     *
     * @param model
     * @param userId
     * @param user
     * @param bindingResult
     * @return
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = {"users/{id}/editUser"}, method = RequestMethod.POST)
    public String editUser(Model model, @PathVariable(value = "id") Integer userId,@Valid @ModelAttribute(value = "user") UserDTO user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError",true);
            return "redirect:/users/" + userId + "/editUser";
        }
        userService.updateUser(userId, user);
        return "redirect:/users";
    }


    /**
     * Get user avatar.
     *
     * @param userId
     * @param response
     * @throws IOException
     * @throws SQLException
     */
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/userAvatar/{userId}"}, method = RequestMethod.GET)
    public void userAvatar(@PathVariable(value = "userId") Integer userId, HttpServletResponse response) throws IOException, SQLException {
        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        InputStream in = ByteSource.wrap(userService.getAvatar(userId)).openStream();
        ByteStreams.copy(in, response.getOutputStream());
        in.close();
    }

    /**
     * Gets role.
     * @return role
     */
    protected static String getRole() {
        String name = "";
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority a : authorities) {
            roles.add(a.getAuthority());
        }
        if (roles.contains("ROLE_ADMIN")) {
            name = "ROLE_ADMIN";
        } else if (roles.contains("ROLE_USER")) {
            name = "ROLE_USER";
        }
        return name;
    }

    /**
     * Get principal.
     * @return user name
     */
    protected static String getPrincipal() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return (principal instanceof User) ? ((User) principal).getUsername(): "";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(UserRoleEntity.class, "roles", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(roleService.getRoleEntityByName(text));
            }
        });

        binder.registerCustomEditor(Date.class, "birthDate", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                if(text.equals("")) {
                    setValue(null);
                }else {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date date = dateFormat.parse(text);
                        setValue(date);
                    } catch (ParseException e) {
                        setValue(null);
                        e.printStackTrace();
                    }
                }
            }
        });
     }

}
