package myLuggage.controller;

import myLuggage.persist.dtos.AccommodationDTO;
import myLuggage.services.IAccommodationService;
import myLuggage.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AccommodationController {

    @Autowired
    private IUserService userService;

    @Autowired
    private IAccommodationService accommodationService;

    @RequestMapping(value = {"/accommodations"}, method = RequestMethod.GET)
    public String accommodation(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("accommodations", accommodationService.getAllAccommodations());
        return "/accommodation/accommodations";
    }

    @RequestMapping(value = {"/addAccommodation"}, method = RequestMethod.GET)
    public String addAccommodation(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("accommodation", new AccommodationDTO());
        return "/accommodation/addAccommodation";
    }

    @RequestMapping(value = {"/addAccommodation"}, method = RequestMethod.POST)
    public String addAccommodation(Model model, @ModelAttribute(value = "accommodation") AccommodationDTO accommodation, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError",true);
            return "redirect:/addAccommodation";
        }
        accommodationService.addAccommodation(accommodation);
        return "redirect:/accommodations";
    }

    @RequestMapping(value = {"accommodations/{id}/deleteAccommodation"}, method = RequestMethod.GET)
    public String deleteAccommodation(@PathVariable(value = "id") Integer id) {
        accommodationService.deleteAccommodation(id);
        return "redirect:/accommodations";
    }

    @RequestMapping(value = {"accommodations/{id}/editAccommodation"}, method = RequestMethod.GET)
    public String editAccommodation(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("accommodation", accommodationService.getAccommodationById(id));
        return "/accommodation/editAccommodation";
    }

    @RequestMapping(value = {"accommodations/{id}/editAccommodation"}, method = RequestMethod.POST)
    public String editAccommodation(Model model, @ModelAttribute(value = "accommodation") AccommodationDTO accommodation, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError",true);
            return "redirect:/editAccommodation";
        }
        accommodationService.updateAccommodation(accommodation);
        return "redirect:/accommodations";
    }



}
