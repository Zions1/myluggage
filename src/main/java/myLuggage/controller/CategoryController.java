package myLuggage.controller;

import myLuggage.persist.dtos.CategoryDTO;
import myLuggage.services.ICategoryService;
import myLuggage.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class CategoryController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ICategoryService categoryService;

    @RequestMapping(value = {"/categories"}, method = RequestMethod.GET)
    public String category(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("categories", categoryService.getAllCategories());
        return "/category/categories";
    }

    @RequestMapping(value = {"/addCategory"}, method = RequestMethod.GET)
    public String addCategory(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("category", new CategoryDTO());
        return "/category/addCategory";
    }

    @RequestMapping(value = {"/addCategory"}, method = RequestMethod.POST)
    public String addCategory(Model model, @ModelAttribute(value = "category") CategoryDTO category, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError",true);
            return "redirect:/addCategory";
        }
        categoryService.addCategory(category);
        return "redirect:/categories";
    }

    @RequestMapping(value = {"categories/{id}/deleteCategory"}, method = RequestMethod.GET)
    public String deleteCategory(@PathVariable(value = "id") Integer id) {
        categoryService.deleteCategory(id);
        return "redirect:/categories";
    }

    @RequestMapping(value = {"categories/{id}/editCategory"}, method = RequestMethod.GET)
    public String editCategory(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("category", categoryService.getCategoryById(id));
        return "/category/editCategory";
    }

    @RequestMapping(value = {"categories/{id}/editCategory"}, method = RequestMethod.POST)
    public String editCategory(Model model, @ModelAttribute(value = "category") CategoryDTO category, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError",true);
            return "redirect:/editCategory";
        }
        categoryService.updateCategory(category);
        return "redirect:/categories";
    }



}
