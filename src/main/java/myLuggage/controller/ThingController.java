package myLuggage.controller;

import myLuggage.exception.ActivityException;
import myLuggage.persist.dtos.ThingDTO;
import myLuggage.persist.model.CategoryEntity;
import myLuggage.persist.model.ActivityEntity;
import myLuggage.persist.model.SeasonEntity;
import myLuggage.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.beans.PropertyEditorSupport;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class ThingController {

    @Autowired
    private IUserService userService;

    @Autowired
    private IThingService thingService;

    @Autowired
    private ICategoryService categoryService;

    @Autowired
    private IActivityService activityService;

    @Autowired
    private ISeasonService seasonService;


    @RequestMapping(value = {"/things"}, method = RequestMethod.GET)
    public String thing(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("things", thingService.getAllThings());
        return "/thing/things";
    }

    @RequestMapping(value = {"/addThing"}, method = RequestMethod.GET)
    public String addThing(Model model) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("thing", new ThingDTO());
        model.addAttribute("categories", categoryService.getAllCategories());
        model.addAttribute("seasons", seasonService.getAllSeasons());
        model.addAttribute("activities", activityService.getAllActivities());
        return "/thing/addThing";
    }

    @RequestMapping(value = {"/addThing"}, method = RequestMethod.POST)
    public String addThing(Model model, @ModelAttribute(value = "thing") ThingDTO thing, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            model.addAttribute("addComponentError",true);
            return "redirect:/addThing";
        }
        thingService.addThing(thing);
        return "redirect:/addThing";
    }

    @RequestMapping(value = {"things/{id}/deleteThing"}, method = RequestMethod.GET)
    public String deleteThing(@PathVariable(value = "id") Integer id) {
        thingService.deleteThing(id);
        return "redirect:/things";
    }

    @RequestMapping(value = {"things/{id}/editThing"}, method = RequestMethod.GET)
    public String editThing(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("user", userService.getUserByEmail(UserController.getPrincipal()));
        model.addAttribute("thing", thingService.getThingById(id));
        return "/thing/editThing";
    }

    @RequestMapping(value = {"things/{id}/editThing"}, method = RequestMethod.POST)
    public String editThing(Model model, @ModelAttribute(value = "thing") ThingDTO thing, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("editComponentError",true);
            return "redirect:/editThing";
        }
        thingService.updateThing(thing);
        return "redirect:/things";
    }


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(CategoryEntity.class, "category", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(categoryService.getCategoryEntityById(Integer.parseInt(text)));
            }
        });

        binder.registerCustomEditor(SeasonEntity.class, "season", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(seasonService.getSeasonEntityByName(text));
            }
        });
        binder.registerCustomEditor(ActivityEntity.class, "activity", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                    setValue(activityService.getActivityEntityById(Integer.parseInt(text)));
            }
        });
    }
}
