package myLuggage.report.excel;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.*;
import myLuggage.persist.dtos.LuggageDTO;
import myLuggage.persist.dtos.TravelDTO;
import myLuggage.persist.model.ActivityEntity;
import myLuggage.persist.model.LuggagesThingsEntity;
import myLuggage.persist.model.PlaceEntity;
import myLuggage.persist.model.TransportEntity;
import org.springframework.web.servlet.view.document.AbstractJExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public class LuggageExcelReportView extends AbstractJExcelView {

    @Override
    protected void buildExcelDocument(Map model, WritableWorkbook workbook,
                                      HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        LuggageDTO luggage = (LuggageDTO) model.get("luggage");
        TravelDTO travel = (TravelDTO) model.get("travel");

        WritableSheet sheet = workbook.createSheet("Luggage Report - " + luggage.getName(), 0);
        WritableFont font = new WritableFont(WritableFont.TIMES, 12);
        WritableFont fontBold = new WritableFont(WritableFont.TIMES, 13, WritableFont.BOLD);

        setTravelDetails(sheet, travel, luggage, font, fontBold);
        setLuggageDetails(sheet, travel, luggage, font, fontBold);
        setPackingList(sheet, travel, luggage, font, fontBold);


        response.setHeader("Content-Disposition", "attachment; filename=\"Luggage Report - " + luggage.getName() + ".xls\"");
    }

    private void setPackingList(WritableSheet sheet, TravelDTO travel, LuggageDTO luggage, WritableFont font, WritableFont fontBold) throws Exception {
        int h = 2;
        sheet.setColumnView(6, 20);
        sheet.setColumnView(7, 20);
        addCell(sheet, Border.NONE, BorderLineStyle.NONE, Alignment.CENTRE, fontBold, 6, 0, "Packing list");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.CENTRE, font, 6, h, "Item");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.CENTRE, font, 7, h++, "Quantity");
        for(LuggagesThingsEntity l : luggage.getThings()){
            addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 6, h, l.getThing().getName());
            addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 7, h++, l.getQuantity().toString());
        }
    }

    private void setLuggageDetails(WritableSheet sheet, TravelDTO travel, LuggageDTO luggage, WritableFont font, WritableFont fontBold) throws Exception {
        int h = 2;
        sheet.setColumnView(3, 20);
        sheet.setColumnView(4, 20);
        addCell(sheet, Border.NONE, BorderLineStyle.NONE, Alignment.CENTRE, fontBold, 3, 0, "Travel details");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 3, h, "Type:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.CENTRE, font, 4, h++, luggage.getType() != null ? luggage.getType().getName() : "NA");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 3, h, "Owner:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.CENTRE, font, 4, h++, luggage.getCategory() != null ? luggage.getCategory().getName() : "NA");
    }

    private void setTravelDetails(WritableSheet sheet, TravelDTO travel, LuggageDTO luggage, WritableFont font, WritableFont fontBold) throws Exception {
        int h = 2;
        sheet.setColumnView(0, 20);
        sheet.setColumnView(1, 20);
        addCell(sheet, Border.NONE, BorderLineStyle.NONE, Alignment.CENTRE, fontBold, 0, 0, "Travel details");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Name:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.CENTRE, font, 1, h++, luggage.getName());
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Start date:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.CENTRE, font, 1, h++, travel.getFormatedStartDate());
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Return date:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.CENTRE, font, 1, h++, travel.getFormatedReturnDate());
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Days:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.CENTRE, font, 1, h++, travel.getDays() != null ? travel.getDays() : "NA");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Continent:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 1, h++, travel.getCountry() != null && travel.getCountry().getContinent() != null ? travel.getCountry().getContinent().getName() : "NA");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Country:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 1, h++, travel.getCountry() != null ? travel.getCountry().getName() : "NA");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Cities:");
        for (PlaceEntity p : travel.getPlaces()) {
            addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 1, h++, p.getName());
            addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "");
        }
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Season:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 1, h++, travel.getSeason() != null ? travel.getSeason().getName() : "NA");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Accommodation:");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 1, h++, travel.getAccommodation() != null ? travel.getAccommodation().getName() : "NA");
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Transports::");
        for (TransportEntity t : travel.getTransports()) {
            addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 1, h++, t.getName());
            addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "");
        }
        addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "Activities:");
        for (ActivityEntity a : travel.getActivities()) {
            addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 1, h++, a.getName());
            addCell(sheet, Border.ALL, BorderLineStyle.THIN, Alignment.LEFT, font, 0, h, "");
        }
        addCell(sheet, Border.ALL, BorderLineStyle.NONE, Alignment.LEFT, font, 0, h, "");
    }


    private void addCell(WritableSheet sheet,
                         Border border,
                         BorderLineStyle borderLineStyle,
                         Alignment alignment,
                         WritableFont font,
                         int col, int row, String desc) throws WriteException {

        WritableCellFormat cellFormat = new WritableCellFormat(font);
        cellFormat.setBorder(border, borderLineStyle);
        cellFormat.setAlignment(alignment);
        Label label = new Label(col, row, desc, cellFormat);
        sheet.addCell(label);
    }


}