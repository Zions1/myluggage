package myLuggage.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@PropertySource("classpath:mail.properties")
public class MailConfiguration {

    @Autowired
    private Environment env;

    @Bean
    public JavaMailSender javaMailService() {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setProtocol(env.getProperty("protocol"));
        sender.setHost(env.getProperty("host"));
        sender.setPort(Integer.parseInt(env.getProperty("port")));
        sender.setUsername(env.getProperty("mailUsername"));
        sender.setPassword(env.getProperty("mailPassword"));
        Properties mailProps = new Properties();
        mailProps.put("mail.smtps.auth", "true");
        mailProps.put("mail.smtp.starttls.enable", "true");
        mailProps.put("mail.smtp.debug", "true");

        sender.setJavaMailProperties(mailProps);
        return sender;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}