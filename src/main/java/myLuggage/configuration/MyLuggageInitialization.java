package myLuggage.configuration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class MyLuggageInitialization implements WebApplicationInitializer {

    private int MAX_UPLOAD_SIZE = 5 * 1024 * 1024;

    public void onStartup(ServletContext servletContext) throws ServletException {

        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
        encodingFilter.setEncoding("UTF-8");
        encodingFilter.setForceEncoding(true);

        //create root context
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(WebMvcConfiguration.class);
        rootContext.register(WebSecurityConfiguration.class);

        //add root context to servlet context as listener
        servletContext.addListener(new ContextLoaderListener(rootContext));

        DispatcherServlet dp =  new DispatcherServlet(rootContext);
        dp.setThrowExceptionIfNoHandlerFound(true); //error not found

        ServletRegistration.Dynamic servlet = servletContext.addServlet(
                "dispatcher", dp
        );


        servlet.setLoadOnStartup(1);

        MultipartConfigElement multipartConfigElement;
        multipartConfigElement = new MultipartConfigElement(null,
                MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 2, MAX_UPLOAD_SIZE / 2);

        servlet.setMultipartConfig(multipartConfigElement);

        servlet.addMapping("/");

        servletContext.addFilter("springSecurityFilterChain", new DelegatingFilterProxy("springSecurityFilterChain"))
                .addMappingForUrlPatterns(null, false, "/*");

        servletContext.addFilter("encoding-filter", encodingFilter);

//        servlet.setInitParameter("throwExceptionIfNoHandlerFound", "true");
    }

}
