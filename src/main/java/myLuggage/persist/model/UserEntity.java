package myLuggage.persist.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user", schema = "my_luggage")
@NamedQueries(
        value = {
                @NamedQuery(name = "getUserByEmail", query = "from UserEntity Where email LIKE :email"),
                @NamedQuery(name = "getUserByUsername", query = "from UserEntity Where username LIKE :username")
        }
)
public class UserEntity {
    private Integer id;

    @NotNull
    private String username;

    @NotNull
    @Size(min = 1, max = 255)
    private String password;

    @Email
    @NotNull
    @Size(min = 1, max = 255)
    private String email;

    private String name;
    private String lastname;
    private Date birthDate;
    private String gender;
    private String token;
    private Boolean active;
    private List<TravelEntity> travels;
    private Collection<UserRoleEntity> roles;
    private UserAvatarEntity avatar;

    @Id
    @Column(name = "user_id", columnDefinition = "serial")
    @SequenceGenerator(name = "user_id_seq",
            sequenceName = "user_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "user_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "lastname")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "birth_date")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Basic
    @Column(name = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "active")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @OneToMany(mappedBy = "user", targetEntity = TravelEntity.class, fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.REMOVE)
    public List<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(List<TravelEntity> travels) {
        this.travels = travels;
    }

    @ManyToMany
    @JoinTable(name = "users_roles", schema = "my_luggage")
    public Collection<UserRoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Collection<UserRoleEntity> roles) {
        this.roles = roles;
    }

    @ManyToOne
    @Cascade({CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "avatar_id")
    public UserAvatarEntity getAvatar() {
        return avatar;
    }

    public void setAvatar(UserAvatarEntity avatar) {
        this.avatar = avatar;
    }

}
