package myLuggage.persist.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "thing", schema = "my_luggage")
public class ThingEntity {
    private Integer id;
    private String name;
    private SeasonEntity season;
    private ActivityEntity activity;
    private CategoryEntity category;
    private List<LuggagesThingsEntity> luggages;

    @Id
    @Column(name = "thing_id", columnDefinition = "serial")
    @SequenceGenerator(name = "thing_id_seq",
            sequenceName = "thing_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "thing_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "season_id")
    public SeasonEntity getSeason() {
        return season;
    }

    public void setSeason(SeasonEntity season) {
        this.season = season;
    }

    @ManyToOne
    @JoinColumn(name = "activity_id")
    public ActivityEntity getActivity() {
        return activity;
    }

    public void setActivity(ActivityEntity activity) {
        this.activity = activity;
    }

    @ManyToOne
    @JoinColumn(name = "category_id")
    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    @OneToMany(mappedBy = "thing", targetEntity = LuggagesThingsEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    public List<LuggagesThingsEntity> getLuggages() {
        return luggages;
    }

    public void setLuggages(List<LuggagesThingsEntity> luggages) {
        this.luggages = luggages;
    }
}
