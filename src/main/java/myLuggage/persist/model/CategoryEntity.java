package myLuggage.persist.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "thing_luggage_category", schema = "my_luggage")
public class CategoryEntity {
    private Integer id;
    private String name;
    private List<LuggageEntity> luggages;
    private List<ThingEntity> things;
    @Id
    @Column(name = "category_id", columnDefinition = "serial")
    @SequenceGenerator(name = "category_id_seq",
            sequenceName = "category_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "category_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "category", targetEntity = LuggageEntity.class, fetch = FetchType.LAZY)
    public List<LuggageEntity> getLuggages() {
        return luggages;
    }

    public void setLuggages(List<LuggageEntity> luggages) {
        this.luggages = luggages;
    }

    @OneToMany(mappedBy = "category", targetEntity = ThingEntity.class, fetch = FetchType.LAZY)
    public List<ThingEntity> getThings() {
        return things;
    }

    public void setThings(List<ThingEntity> things) {
        this.things = things;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryEntity that = (CategoryEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (luggages != null ? !luggages.equals(that.luggages) : that.luggages != null) return false;
        return things != null ? things.equals(that.things) : that.things == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (luggages != null ? luggages.hashCode() : 0);
        result = 31 * result + (things != null ? things.hashCode() : 0);
        return result;
    }
}
