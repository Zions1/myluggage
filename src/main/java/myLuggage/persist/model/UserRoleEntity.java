package myLuggage.persist.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "user_role", schema = "my_luggage")
@NamedQueries(
        value = {
                @NamedQuery(name = "findRoleByName", query = "from UserRoleEntity WHERE upper(role) like :name")
        }
)
public class UserRoleEntity {
    private Integer id;
    private String role;
    private Collection<UserEntity> users;

    @Id
    @Column(name = "role_id", columnDefinition = "serial")
    @SequenceGenerator(name = "role_id_seq",
            sequenceName = "role_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "role_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @ManyToMany(mappedBy = "roles")
    public Collection<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(Collection<UserEntity> user){
        this.users=user;
    }

 }
