package myLuggage.persist.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "place", schema = "my_luggage")
public class PlaceEntity {
    private Integer id;
    private String name;
    private Collection<TravelEntity> travels;
    private CountryEntity country;

    @Id
    @Column(name = "place_id", columnDefinition = "serial")
    @SequenceGenerator(name = "place_id_seq",
            sequenceName = "place_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "place_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @JoinColumn(name = "country_id")
    public CountryEntity getCountry() {
        return country;
    }

    public void setCountry(CountryEntity country) {
        this.country = country;
    }

    @ManyToMany(mappedBy = "places", targetEntity = TravelEntity.class, fetch = FetchType.LAZY)
    public Collection<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(Collection<TravelEntity> travels) {
        this.travels = travels;
    }
}
