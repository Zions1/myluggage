package myLuggage.persist.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "travel_dining_option", schema = "my_luggage")
public class DiningOptionEntity {
    private Integer id;
    private String name;
    private List<TravelEntity> travels;

    @Id
    @Column(name = "dining_option_id", columnDefinition = "serial")
    @SequenceGenerator(name = "dining_option_id_seq",
            sequenceName = "dining_option_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "dining_option_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "diningOption", targetEntity = TravelEntity.class, fetch = FetchType.LAZY)
    public List<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(List<TravelEntity> travels) {
        this.travels = travels;
    }
}
