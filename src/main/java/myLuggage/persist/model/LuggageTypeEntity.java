package myLuggage.persist.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "luggage_type", schema = "my_luggage")
public class LuggageTypeEntity {
    private Integer id;
    private String name;
    private List<LuggageEntity> luggages;

    @Id
    @Column(name = "type_id", columnDefinition = "serial")
    @SequenceGenerator(name = "type_id_seq",
            sequenceName = "type_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "type_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "type", targetEntity = LuggageEntity.class, fetch = FetchType.LAZY)
    public List<LuggageEntity> getLuggages() {
        return luggages;
    }

    public void setLuggages(List<LuggageEntity> luggages) {
        this.luggages = luggages;
    }
}
