package myLuggage.persist.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "luggages_things", schema = "my_luggage")
@NamedQueries(
        value = {
                @NamedQuery(name = "findLuggageThingById", query = "from LuggagesThingsEntity WHERE luggage.id = :luggageId AND thing.id = :thingId ")
        }
)
public class LuggagesThingsEntity implements Serializable {
    private LuggageEntity luggage;
    private ThingEntity thing;
    private Integer quantity;

    @Id
    @ManyToOne
    @JoinColumn(name = "luggage_id")
    public LuggageEntity getLuggage() {
        return luggage;
    }

    public void setLuggage(LuggageEntity luggage) {
        this.luggage = luggage;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "thing_id")
    public ThingEntity getThing() {
        return thing;
    }

    public void setThing(ThingEntity thing) {
        this.thing = thing;
    }

    @Column(name = "quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LuggagesThingsEntity entity = (LuggagesThingsEntity) o;

        if (luggage != null ? !luggage.equals(entity.luggage) : entity.luggage != null) return false;
        if (thing != null ? !thing.equals(entity.thing) : entity.thing != null) return false;
        return quantity != null ? quantity.equals(entity.quantity) : entity.quantity == null;

    }

    @Override
    public int hashCode() {
        int result = luggage != null ? luggage.hashCode() : 0;
        result = 31 * result + (thing != null ? thing.hashCode() : 0);
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        return result;
    }
}
