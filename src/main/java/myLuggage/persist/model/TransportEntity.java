package myLuggage.persist.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "travel_transport", schema = "my_luggage")
public class TransportEntity {
    private Integer id;
    private String name;
    private Collection<TravelEntity> travels;

    @Id
    @Column(name = "transport_id", columnDefinition = "serial")
    @SequenceGenerator(name = "transport_id_seq",
            sequenceName = "transport_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "transport_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "transports", cascade = CascadeType.REMOVE)
    public Collection<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(Collection<TravelEntity> travels) {
        this.travels = travels;
    }
}
