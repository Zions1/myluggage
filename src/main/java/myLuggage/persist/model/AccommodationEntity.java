package myLuggage.persist.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "travel_accommodation", schema = "my_luggage")
public class AccommodationEntity {
    private Integer id;
    private String name;
    private List<TravelEntity> travels;

    @Id
    @Column(name = "accommodation_id", columnDefinition = "serial")
    @SequenceGenerator(name = "accommodation_id_seq",
            sequenceName = "accommodation_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "accommodation_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "accommodation", targetEntity = TravelEntity.class, fetch = FetchType.LAZY)
    public List<TravelEntity> getTravels() {        return travels;
    }

    public void setTravels(List<TravelEntity> travels) {
        this.travels = travels;
    }
}
