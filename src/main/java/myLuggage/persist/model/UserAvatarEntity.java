package myLuggage.persist.model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Blob;
import java.util.List;

@Entity
@Table(name = "user_avatar", schema = "my_luggage")
public class UserAvatarEntity {
    private Integer id;
    private Blob avatar;
    private byte[] avatarBinaries;
    private List<UserEntity> users;

    @Id
    @Column(name = "avatar_id", columnDefinition = "serial")
    @SequenceGenerator(name = "avatar_id_seq",
            sequenceName = "avatar_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "avatar_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Lob
    @Column(name = "avatar")
    public Blob getAvatar() {
        return avatar;
    }

    public void setAvatar(Blob avatar) {
        this.avatar = avatar;
    }

    @Transient
    public byte[] getAvatarBinaries() {
        return avatarBinaries;
    }

    public void setAvatarBinaries(byte[] avatarBinaries) {
        this.avatarBinaries = avatarBinaries;
    }

    @OneToMany(mappedBy = "avatar", targetEntity = UserEntity.class, fetch = FetchType.LAZY)
    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

}
