package myLuggage.persist.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "luggage", schema = "my_luggage")
public class LuggageEntity {
    private Integer id;
    private String name;
    private TravelEntity travel;
    private CategoryEntity category;
    private LuggageTypeEntity type;
    private List<LuggagesThingsEntity> things;

    @Id
    @Column(name = "luggage_id", columnDefinition = "serial")
    @SequenceGenerator(name = "luggage_id_seq",
            sequenceName = "luggage_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "luggage_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "travel_id")
    public TravelEntity getTravel() {
        return travel;
    }

    public void setTravel(TravelEntity travel) {
        this.travel = travel;
    }

    @ManyToOne
    @JoinColumn(name = "category_id")
    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    @ManyToOne
    @JoinColumn(name = "type_id")
    public LuggageTypeEntity getType() {
        return type;
    }

    public void setType(LuggageTypeEntity type) {
        this.type = type;
    }

    @OneToMany(mappedBy = "luggage", targetEntity = LuggagesThingsEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    public List<LuggagesThingsEntity> getThings() {
        return things;
    }

    public void setThings(List<LuggagesThingsEntity> things) {
        this.things = things;
    }
}
