package myLuggage.persist.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "travel_thing_activity", schema = "my_luggage")
public class ActivityEntity {
    private Integer id;
    private String name;
    private Collection<TravelEntity> travels;
    private List<ThingEntity> things;

    @Id
    @Column(name = "activity_id", columnDefinition = "serial")
    @SequenceGenerator(name = "activity_id_seq",
            sequenceName = "activity_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "activity_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "activities", cascade = CascadeType.REMOVE)
    public Collection<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(Collection<TravelEntity> travels) {
        this.travels = travels;
    }

    @OneToMany(mappedBy = "activity", targetEntity = ThingEntity.class, fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    public List<ThingEntity> getThings() {
        return things;
    }

    public void setThings(List<ThingEntity> things) {
        this.things = things;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActivityEntity that = (ActivityEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (travels != null ? !travels.equals(that.travels) : that.travels != null) return false;
        return things != null ? things.equals(that.things) : that.things == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (travels != null ? travels.hashCode() : 0);
        result = 31 * result + (things != null ? things.hashCode() : 0);
        return result;
    }
}
