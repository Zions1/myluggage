package myLuggage.persist.model;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "travel_country", schema = "my_luggage")
public class CountryEntity {
    private Integer id;
    private String code;
    private String name;
    private String fullName;
    private ContinentEntity continent;
    private List<PlaceEntity> places;

    @Id
    @Column(name = "country_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "fullname")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @ManyToOne
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @JoinColumn(name = "continent_id")
    public ContinentEntity getContinent() {
        return continent;
    }

    public void setContinent(ContinentEntity continent) {
        this.continent = continent;
    }


    @OneToMany(mappedBy = "country", targetEntity = PlaceEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public List<PlaceEntity> getPlaces() {
        return places;
    }

    public void setPlaces(List<PlaceEntity> places) {
        this.places = places;
    }
}
