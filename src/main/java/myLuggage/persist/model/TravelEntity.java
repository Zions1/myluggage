package myLuggage.persist.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "travel", schema = "my_luggage")
public class TravelEntity {
    private Integer id;
    private String name;
    private Date startDate;
    private Date returnDate;
    private DiningOptionEntity diningOption;
    private AccommodationEntity accommodation;
    private UserEntity user;
    private SeasonEntity season;
    private List<LuggageEntity> luggages;
    private Collection<TransportEntity> transports;
    private Collection<ActivityEntity> activities;
    private Collection<PlaceEntity> places;

    @Id
    @Column(name = "travel_id", columnDefinition = "serial")
    @SequenceGenerator(name = "travel_id_seq",
            sequenceName = "travel_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "travel_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "start_date")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Column(name = "return_date")
    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @ManyToOne
    @JoinColumn(name = "dining_option_id")
    public DiningOptionEntity getDiningOption() {
        return diningOption;
    }

    public void setDiningOption(DiningOptionEntity diningOption) {
        this.diningOption = diningOption;
    }

    @ManyToOne
    @JoinColumn(name = "accommodation_id")
    public AccommodationEntity getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(AccommodationEntity accommodation) {
        this.accommodation = accommodation;
    }

    @ManyToOne
    @Cascade({CascadeType.DETACH})
    @JoinColumn(name = "user_id")
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name = "season_id")
    public SeasonEntity getSeason() {
        return season;
    }

    public void setSeason(SeasonEntity season) {
        this.season = season;
    }

    @OneToMany(mappedBy = "travel", targetEntity = LuggageEntity.class, fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.REMOVE)
    public List<LuggageEntity> getLuggages() {
        return luggages;
    }

    public void setLuggages(List<LuggageEntity> luggages) {
        this.luggages = luggages;
    }

    @ManyToMany
    @JoinTable(name = "travels_transports", schema = "my_luggage")
    public Collection<TransportEntity> getTransports() {
        return transports;
    }

    public void setTransports(Collection<TransportEntity> transports) {
        this.transports = transports;
    }

    @ManyToMany
    @JoinTable(name = "travels_activities", schema = "my_luggage")
    public Collection<ActivityEntity> getActivities() {
        return activities;
    }

    public void setActivities(Collection<ActivityEntity> activities) {
        this.activities = activities;
    }

    @ManyToMany
    @JoinTable(name = "travels_places", schema = "my_luggage")
    public Collection<PlaceEntity> getPlaces() {
        return places;
    }

    public void setPlaces(Collection<PlaceEntity> places) {
        this.places = places;
    }
}
