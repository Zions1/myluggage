package myLuggage.persist.model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "travel_continent", schema = "my_luggage")
public class ContinentEntity {
    private Integer id;
    private String code;
    private String name;
    private List<CountryEntity> continents;

    @Id
    @Column(name = "travel_continent_id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "continent", targetEntity = CountryEntity.class, fetch = FetchType.LAZY)
    public List<CountryEntity> getContinents() {
        return continents;
    }

    public void setContinents(List<CountryEntity> continents) {
        this.continents = continents;
    }

}
