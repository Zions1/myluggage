package myLuggage.persist.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "travel_thing_season", schema = "my_luggage")
public class SeasonEntity {
    private Integer id;
    private String name;
    private List<ThingEntity> things;
    private List<TravelEntity> travels;

    @Id
    @Column(name = "season_id", columnDefinition = "serial")
    @SequenceGenerator(name = "season_id_seq",
            sequenceName = "season_id_seq",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY,
            generator = "season_id_seq")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "season", targetEntity = ThingEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    public List<ThingEntity> getThings() {
        return things;
    }

    public void setThings(List<ThingEntity> things) {
        this.things = things;
    }

    @OneToMany(mappedBy = "season", targetEntity = TravelEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    public List<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(List<TravelEntity> travels) {
        this.travels = travels;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SeasonEntity that = (SeasonEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (things != null ? !things.equals(that.things) : that.things != null) return false;
        return travels != null ? travels.equals(that.travels) : that.travels == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (things != null ? things.hashCode() : 0);
        result = 31 * result + (travels != null ? travels.hashCode() : 0);
        return result;
    }
}
