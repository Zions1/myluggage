package myLuggage.persist.dtos;

import myLuggage.persist.model.TravelEntity;

import java.util.Collection;

/**
 * Data Transfer Object class for transport.
 *
 * @author Wiktor Florencki
 */
public class TransportDTO {
    private Integer id;
    private String name;
    private Collection<TravelEntity> travels;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(Collection<TravelEntity> travels) {
        this.travels = travels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransportDTO that = (TransportDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return travels != null ? travels.equals(that.travels) : that.travels == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (travels != null ? travels.hashCode() : 0);
        return result;
    }
}
