package myLuggage.persist.dtos;

import myLuggage.persist.model.*;

import java.util.List;

/**
 * Data Transfer Object class for luggage.
 *
 * @author Wiktor Florencki
 */
public class LuggageDTO {
    private Integer id;
    private String name;
    private TravelEntity travel;
    private CategoryEntity category;
    private LuggageTypeEntity type;
    private List<LuggagesThingsEntity> things;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TravelEntity getTravel() {
        return travel;
    }

    public void setTravel(TravelEntity travel) {
        this.travel = travel;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public LuggageTypeEntity getType() {
        return type;
    }

    public void setType(LuggageTypeEntity type) {
        this.type = type;
    }

    public List<LuggagesThingsEntity> getThings() {
        return things;
    }

    public void setThings(List<LuggagesThingsEntity> things) {
        this.things = things;
    }

    public Integer thingsQuantity(){
        return things.size();
    }
}
