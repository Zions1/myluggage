package myLuggage.persist.dtos;

import myLuggage.persist.model.LuggageEntity;
import myLuggage.persist.model.ThingEntity;

import java.util.List;

/**
 * Data Transfer Object class for category.
 *
 * @author Wiktor Florencki
 */
public class CategoryDTO {
    private Integer id;
    private String name;
    private List<LuggageEntity> luggages;
    private List<ThingEntity> things;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LuggageEntity> getLuggages() {
        return luggages;
    }

    public void setLuggages(List<LuggageEntity> luggages) {
        this.luggages = luggages;
    }

    public List<ThingEntity> getThings() {
        return things;
    }

    public void setThings(List<ThingEntity> things) {
        this.things = things;
    }

    public String getNameWithoutSpace(){
        return name.replaceAll("\\s","").replaceAll("&","And");
    }
}
