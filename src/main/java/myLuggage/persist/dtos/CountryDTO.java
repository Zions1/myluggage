package myLuggage.persist.dtos;

import myLuggage.persist.model.ContinentEntity;
import myLuggage.persist.model.PlaceEntity;

import java.util.List;

/**
 * Data Transfer Object class for country.
 *
 * @author Wiktor Florencki
 */
public class CountryDTO {
    private Integer id;
    private String code;
    private String name;
    private String fullName;
    private ContinentEntity continent;
    private List<PlaceEntity> places;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public ContinentEntity getContinent() {
        return continent;
    }

    public void setContinent(ContinentEntity continent) {
        this.continent = continent;
    }

    public List<PlaceEntity> getPlaces() {
        return places;
    }

    public void setPlaces(List<PlaceEntity> places) {
        this.places = places;
    }
}
