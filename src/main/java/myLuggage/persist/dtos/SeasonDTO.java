package myLuggage.persist.dtos;

import myLuggage.persist.model.ThingEntity;
import myLuggage.persist.model.TravelEntity;

import java.util.List;

/**
 * Data Transfer Object class for season.
 *
 * @author Wiktor Florencki
 */
public class SeasonDTO {
    private Integer id;
    private String name;
    private List<ThingEntity> things;
    private List<TravelEntity> travels;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ThingEntity> getThings() {
        return things;
    }

    public void setThings(List<ThingEntity> things) {
        this.things = things;
    }

    public List<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(List<TravelEntity> travels) {
        this.travels = travels;
    }
}

