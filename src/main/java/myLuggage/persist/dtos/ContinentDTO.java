package myLuggage.persist.dtos;

import myLuggage.persist.model.CountryEntity;

import java.util.List;

/**
 * Data Transfer Object class for continent.
 *
 * @author Wiktor Florencki
 */
public class ContinentDTO {
    private Integer id;
    private String code;
    private String name;
    private List<CountryEntity> continents;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CountryEntity> getContinents() {
        return continents;
    }

    public void setContinents(List<CountryEntity> continents) {
        this.continents = continents;
    }

}
