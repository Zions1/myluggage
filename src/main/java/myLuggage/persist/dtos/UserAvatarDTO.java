package myLuggage.persist.dtos;

import myLuggage.persist.model.UserEntity;

import java.sql.Blob;
import java.util.List;

/**
 * Data Transfer Object class for avatar.
 *
 * @author Wiktor Florencki
 */
public class UserAvatarDTO {
    private Integer id;
    private Blob avatar;
    private List<UserEntity> users;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Blob getAvatar() {
        return avatar;
    }

    public void setAvatar(Blob avatar) {
        this.avatar = avatar;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }
}
