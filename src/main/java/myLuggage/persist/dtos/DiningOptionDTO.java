package myLuggage.persist.dtos;

import myLuggage.persist.model.TravelEntity;

import java.util.List;

/**
 * Data Transfer Object class for dining option.
 *
 * @author Wiktor Florencki
 */
public class DiningOptionDTO {
    private Integer id;
    private String name;
    private List<TravelEntity> travels;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(List<TravelEntity> travels) {
        this.travels = travels;
    }
}
