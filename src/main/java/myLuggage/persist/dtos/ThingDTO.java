package myLuggage.persist.dtos;

import myLuggage.persist.model.*;

import java.util.List;

/**
 * Data Transfer Object class for thing.
 *
 * @author Wiktor Florencki
 */
public class ThingDTO {
    private Integer id;
    private String name;
    private SeasonEntity season;
    private ActivityEntity activity;
    private CategoryEntity category;
    private List<LuggagesThingsEntity> luggages;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SeasonEntity getSeason() {
        return season;
    }

    public void setSeason(SeasonEntity season) {
        this.season = season;
    }

    public ActivityEntity getActivity() {
        return activity;
    }

    public void setActivity(ActivityEntity activity) {
        this.activity = activity;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public List<LuggagesThingsEntity> getLuggages() {
        return luggages;
    }

    public void setLuggages(List<LuggagesThingsEntity> luggages) {
        this.luggages = luggages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThingDTO thingDTO = (ThingDTO) o;

        if (id != null ? !id.equals(thingDTO.id) : thingDTO.id != null) return false;
        if (name != null ? !name.equals(thingDTO.name) : thingDTO.name != null) return false;
        if (season != null ? !season.equals(thingDTO.season) : thingDTO.season != null) return false;
        if (activity != null ? !activity.equals(thingDTO.activity) : thingDTO.activity != null) return false;
        if (category != null ? !category.equals(thingDTO.category) : thingDTO.category != null) return false;
        return luggages != null ? luggages.equals(thingDTO.luggages) : thingDTO.luggages == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (season != null ? season.hashCode() : 0);
        result = 31 * result + (activity != null ? activity.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (luggages != null ? luggages.hashCode() : 0);
        return result;
    }
}
