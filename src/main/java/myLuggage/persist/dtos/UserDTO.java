package myLuggage.persist.dtos;

import myLuggage.persist.model.UserAvatarEntity;
import myLuggage.persist.model.UserRoleEntity;
import myLuggage.persist.model.TravelEntity;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Data Transfer Object class for user.
 *
 * @author Wiktor Florencki
 */
public class UserDTO {
    private Integer id;
    private String username;
    private String password;
    private String email;
    private String name;
    private String lastname;
    private Date birthDate;
    private String gender;
    private String token;
    private Boolean active;
    private List<TravelEntity> travels;
    private Collection<UserRoleEntity> roles;
    private UserAvatarEntity avatar;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Collection<UserRoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Collection<UserRoleEntity> roles) {
        this.roles = roles;
    }

    public UserAvatarEntity getAvatar() {
        return avatar;
    }

    public void setAvatar(UserAvatarEntity avatar) {
        this.avatar = avatar;
    }

    public List<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(List<TravelEntity> travels) {
        this.travels = travels;
    }

    public String getOnlyBirthDayDate() {
        if (this.getBirthDate() != null) {
            SimpleDateFormat sm = new SimpleDateFormat("dd.MM.yyyy");
            return sm.format(this.getBirthDate());
        }
        return "";
    }
}
