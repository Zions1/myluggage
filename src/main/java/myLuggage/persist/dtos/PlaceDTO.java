package myLuggage.persist.dtos;

import myLuggage.persist.model.CountryEntity;
import myLuggage.persist.model.TravelEntity;

import java.util.Collection;

/**
 * Data Transfer Object class for place.
 *
 * @author Wiktor Florencki
 */
public class PlaceDTO {
    private Integer id;
    private String name;
    private Collection<TravelEntity> travels;
    private CountryEntity country;

    public PlaceDTO(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<TravelEntity> getTravels() {
        return travels;
    }

    public void setTravels(Collection<TravelEntity> travels) {
        this.travels = travels;
    }

    public CountryEntity getCountry() {
        return country;
    }

    public void setCountry(CountryEntity country) {
        this.country = country;
    }
}
