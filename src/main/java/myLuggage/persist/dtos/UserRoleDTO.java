package myLuggage.persist.dtos;


import myLuggage.persist.model.UserEntity;

import java.util.Collection;

/**
 * Data Transfer Object class for role.
 *
 * @author Wiktor Florencki
 */
public class UserRoleDTO {

    private Integer id;
    private String role;
    private Collection<UserEntity> users;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Collection<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(Collection<UserEntity> users) {
        this.users = users;
    }
}
