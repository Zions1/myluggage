package myLuggage.persist.dtos;

import myLuggage.persist.model.LuggageEntity;
import myLuggage.persist.model.ThingEntity;

/**
 * Data Transfer Object class for luggage thing.
 *
 * @author Wiktor Florencki
 */
public class LuggagesThingsDTO {
    private LuggageEntity luggage;
    private ThingEntity thing;
    private Integer quantity;

    public LuggageEntity getLuggage() {
        return luggage;
    }

    public void setLuggage(LuggageEntity luggage) {
        this.luggage = luggage;
    }

    public ThingEntity getThing() {
        return thing;
    }

    public void setThing(ThingEntity thing) {
        this.thing = thing;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
