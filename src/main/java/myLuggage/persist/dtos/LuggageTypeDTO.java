package myLuggage.persist.dtos;

import myLuggage.persist.model.LuggageEntity;

import java.util.List;

/**
 * Data Transfer Object class for luggage type.
 *
 * @author Wiktor Florencki
 */
public class LuggageTypeDTO {
    private Integer id;
    private String name;
    private List<LuggageEntity> luggages;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LuggageEntity> getLuggages() {
        return luggages;
    }

    public void setLuggages(List<LuggageEntity> luggages) {
        this.luggages = luggages;
    }
}
