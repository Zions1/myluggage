package myLuggage.persist.dtos;

import myLuggage.persist.model.*;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Data Transfer Object class for travel.
 *
 * @author Wiktor Florencki
 */
public class TravelDTO {
    private Integer id;
    private String name;
    private Date startDate;
    private Date returnDate;
    private DiningOptionEntity diningOption;
    private AccommodationEntity accommodation;
    private UserEntity user;
    private SeasonEntity season;
    private List<LuggageEntity> luggages;
    private Collection<TransportEntity> transports;
    private Collection<ActivityEntity> activities;
    private Collection<PlaceEntity> places;
    private CountryEntity country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public DiningOptionEntity getDiningOption() {
        return diningOption;
    }

    public void setDiningOption(DiningOptionEntity diningOption) {
        this.diningOption = diningOption;
    }

    public AccommodationEntity getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(AccommodationEntity accommodation) {
        this.accommodation = accommodation;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Collection<ActivityEntity> getActivities() {
        return activities;
    }

    public void setActivities(Collection<ActivityEntity> activities) {
        this.activities = activities;
    }

    public SeasonEntity getSeason() {
        return season;
    }

    public void setSeason(SeasonEntity season) {
        this.season = season;
    }

    public Collection<TransportEntity> getTransports() {
        return transports;
    }

    public void setTransports(Collection<TransportEntity> transports) {
        this.transports = transports;
    }

    public List<LuggageEntity> getLuggages() {
        return luggages;
    }

    public void setLuggages(List<LuggageEntity> luggages) {
        this.luggages = luggages;
    }

    public Collection<PlaceEntity> getPlaces() {
        return places;
    }

    public void setPlaces(Collection<PlaceEntity> places) {
        this.places = places;
    }

    public CountryEntity getCountry() {
        return country;
    }

    public void setCountry(CountryEntity country) {
        this.country = country;
    }

    public String getFormatedStartDate() {
        if (this.getStartDate() != null) {
            SimpleDateFormat sm = new SimpleDateFormat("dd-MM-yyyy");
            return sm.format(this.getStartDate());
        }
        return "NA";
    }

    public String getFormatedReturnDate() {
        if (this.getReturnDate() != null) {
            SimpleDateFormat sm = new SimpleDateFormat("dd-MM-yyyy");
            return sm.format(this.getReturnDate());
        }
        return "NA";
    }

    public String getDays() {
        if (this.getStartDate() != null && this.getReturnDate() != null) {
            Long diff = this.getReturnDate().getTime() - this.getStartDate().getTime();
            return String.valueOf(diff / (24 * 60 * 60 * 1000));
        }
        return "NA";
    }


}
