package myLuggage.persist.daos.implementationsDAO;

import myLuggage.exception.RepositoryException;
import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;


public abstract class BaseDAO<T, K extends Serializable> implements IBaseDAO<T, K> {

    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> clazz;

    @SuppressWarnings("unchecked")
    public BaseDAO() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.clazz = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }


    @Override
    public void create(T t) {
        currentSession().save(t);
    }

    @Override
    public T findByName(String name) {
        return this.getCriteria().add(Restrictions.eq("name", name)).uniqueResult() != null ?
                (T) this.getCriteria().add(Restrictions.eq("name", name)).uniqueResult() : null;
    }

    @Override
    public T getById(K key) {
        return currentSession().get(clazz, key) != null ? (T) currentSession().get(clazz, key) : null;
    }

    @Override
    public void delete(T t)  {
       currentSession().delete(t);

    }

    @Override
    public void deleteById(K id) {
        if (this.getById(id) != null) {
            currentSession().delete(this.getById(id));
        }
    }

    @Override
    public void update(T t) {
        currentSession().saveOrUpdate(t);
    }

    @Override
    public void merge(T t) {
        currentSession().merge(t);
    }

    @Override
    public List<T> getAll() {
        return currentSession().createCriteria(clazz).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    protected Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    protected Criteria getCriteria() {
        return currentSession().createCriteria(clazz);
    }

    protected Query getNamedQuery(String s) {
        return currentSession().getNamedQuery(s);
    }


}
