package myLuggage.persist.daos.implementationsDAO;

import myLuggage.persist.daos.interfacesDAO.IActivityDAO;
import myLuggage.persist.model.ActivityEntity;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ActivityDAO extends BaseDAO<ActivityEntity, Integer> implements IActivityDAO {

    @Override
    public List<ActivityEntity> getAllActivitiesWithoutBasic() {
        Criteria criteria = super.getCriteria();
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .add(Restrictions.not(Restrictions.like("name", "Essentials")))
                .add(Restrictions.not(Restrictions.like("name", "Toiletries")))
                .add(Restrictions.not(Restrictions.like("name", "Free time")));
        return criteria.list();
    }
}
