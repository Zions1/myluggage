package myLuggage.persist.daos.implementationsDAO.thing;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.thing.IThingDAO;
import myLuggage.persist.model.ThingEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ThingDAO extends BaseDAO<ThingEntity, Integer> implements IThingDAO {

}
