package myLuggage.persist.daos.implementationsDAO.travel;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.travel.ITransportDAO;
import myLuggage.persist.model.TransportEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class TransportDAO extends BaseDAO<TransportEntity, Integer> implements ITransportDAO {

}
