package myLuggage.persist.daos.implementationsDAO.travel;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.travel.IContinentDAO;
import myLuggage.persist.model.ContinentEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ContinentDAO extends BaseDAO<ContinentEntity, Integer> implements IContinentDAO {

}
