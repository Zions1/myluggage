package myLuggage.persist.daos.implementationsDAO.travel;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.travel.IAccommodationDAO;
import myLuggage.persist.model.AccommodationEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class AccommodationDAO extends BaseDAO<AccommodationEntity, Integer> implements IAccommodationDAO {

}
