package myLuggage.persist.daos.implementationsDAO.travel;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.travel.IPlaceDAO;
import myLuggage.persist.model.PlaceEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class PlaceDAO extends BaseDAO<PlaceEntity, Integer> implements IPlaceDAO {

}
