package myLuggage.persist.daos.implementationsDAO.travel;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.travel.ITravelDAO;
import myLuggage.persist.model.TravelEntity;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TravelDAO extends BaseDAO<TravelEntity, Integer> implements ITravelDAO {

    @Override
    public List<TravelEntity> getAllUserTravels(int id) {
       return super.getCriteria().add(Restrictions.eq("user.id", id)).list();
    }

    @Override
    public Integer getLastId() {
        return (Integer) super.getCriteria().setProjection(Projections.max("id")).uniqueResult();
    }
}
