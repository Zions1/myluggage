package myLuggage.persist.daos.implementationsDAO.travel;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.travel.ICountryDAO;
import myLuggage.persist.model.CountryEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class CountryDAO extends BaseDAO<CountryEntity, Integer> implements ICountryDAO {

}
