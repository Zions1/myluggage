package myLuggage.persist.daos.implementationsDAO.travel;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.travel.IDiningOptionDAO;
import myLuggage.persist.model.DiningOptionEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class DiningOptionDAO extends BaseDAO<DiningOptionEntity, Integer> implements IDiningOptionDAO {
}
