package myLuggage.persist.daos.implementationsDAO.user;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserRoleDAO;
import myLuggage.persist.model.UserRoleEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleDAO extends BaseDAO<UserRoleEntity, Integer> implements IUserRoleDAO {

    @Override
    public UserRoleEntity findByName(String name) {
        Query q = super.getNamedQuery("findRoleByName").setParameter("name", name.toUpperCase());
        UserRoleEntity roleEntity = (UserRoleEntity) q.uniqueResult();
        if (roleEntity != null) {
            return roleEntity;
        }
        return null;
    }
}
