package myLuggage.persist.daos.implementationsDAO.user;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserAvatarDAO;
import myLuggage.persist.model.UserAvatarEntity;
import org.springframework.stereotype.Repository;

@Repository
public class UserAvatarDAO extends BaseDAO<UserAvatarEntity, Integer> implements IUserAvatarDAO {

}
