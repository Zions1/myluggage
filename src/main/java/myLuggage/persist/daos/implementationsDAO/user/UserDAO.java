package myLuggage.persist.daos.implementationsDAO.user;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserDAO;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import myLuggage.persist.model.UserEntity;

import java.util.UUID;

@Repository
public class UserDAO extends BaseDAO<UserEntity, Integer> implements IUserDAO {

    @Autowired
    private PasswordEncoder passwordEncoder;



    @Override
    public UserEntity getUserByEmail(String email) {
        Query q = super.getNamedQuery("getUserByEmail").setParameter("email", email);
        UserEntity userEntity = (UserEntity) q.uniqueResult();
        if (userEntity != null) {
            return userEntity;
        }
        return null;
    }

    @Override
    public UserEntity getUserByUsername(String username) {
        Query q = super.getNamedQuery("getUserByUsername").setParameter("username", username);
        UserEntity userEntity = (UserEntity) q.uniqueResult();
        if (userEntity != null) {
            return userEntity;
        }
        return null;
    }

    @Override
    public void addUser(UserEntity user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setToken(UUID.randomUUID().toString());
        super.create(user);
    }

}
