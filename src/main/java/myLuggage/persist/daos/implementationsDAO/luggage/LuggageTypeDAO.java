package myLuggage.persist.daos.implementationsDAO.luggage;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.luggage.ILuggageTypeDAO;
import myLuggage.persist.model.LuggageTypeEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LuggageTypeDAO extends BaseDAO<LuggageTypeEntity, Integer> implements ILuggageTypeDAO {

}
