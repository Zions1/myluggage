package myLuggage.persist.daos.implementationsDAO.luggage;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.luggage.ILuggageDAO;
import myLuggage.persist.model.ActivityEntity;
import myLuggage.persist.model.LuggageEntity;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LuggageDAO extends BaseDAO<LuggageEntity, Integer> implements ILuggageDAO {

    @Override
    public List<LuggageEntity> getAllUserLuggages(Integer userId) {
        Criteria criteria = super.getCriteria()
                .createAlias("travel.user", "u")
                .add(Restrictions.eq("u.id", userId));
        return criteria.list();
    }
}
