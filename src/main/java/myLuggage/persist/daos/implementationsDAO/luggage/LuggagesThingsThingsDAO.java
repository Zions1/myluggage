package myLuggage.persist.daos.implementationsDAO.luggage;

import myLuggage.persist.daos.implementationsDAO.BaseDAO;
import myLuggage.persist.daos.interfacesDAO.luggage.ILuggagesThingsDAO;
import myLuggage.persist.model.LuggagesThingsEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class LuggagesThingsThingsDAO extends BaseDAO<LuggagesThingsEntity, Integer> implements ILuggagesThingsDAO {

    @Override
    public void deleteLuggagesThings(int luggageId, int thingId) {
        Query q = super.getNamedQuery("findLuggageThingById").setParameter("luggageId", luggageId).setParameter("thingId", thingId);
        LuggagesThingsEntity luggage = (LuggagesThingsEntity) q.uniqueResult();
        if (luggage != null) {
            super.delete(luggage);
        }
    }
}
