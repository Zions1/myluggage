package myLuggage.persist.daos.implementationsDAO;

import myLuggage.persist.daos.interfacesDAO.ICategoryDAO;
import myLuggage.persist.model.CategoryEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDAO extends BaseDAO<CategoryEntity, Integer> implements ICategoryDAO {

}