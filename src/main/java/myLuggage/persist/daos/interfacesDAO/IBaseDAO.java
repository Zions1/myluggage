package myLuggage.persist.daos.interfacesDAO;

import myLuggage.exception.RepositoryException;

import java.util.List;

/**
 * Interface of Repository to handle CRUD operation, such as storing, retrieving, updating and deleting data.
 *
 * @author Wiktor Florencki
 */
public interface IBaseDAO<T, K> {

    /**
     * Finds object by id.
     * @param id technical identifier
     * @return object
     */
    T getById(K id) ;

    /**
     * Finds object by column named 'name'.
     * @param name column name
     * @return object
     */
    T findByName(String name) ;

    /**
     * Create object.
     * @param t object
     */
    void create(T t);

    /**
     * Update object.
     * @param t object
     */
    void update(T t);

    /**
     * Merge object.
     * @param t object
     */
    void merge(T t);

    /**
     * Delete object.
     * @param t object
     */
    void delete(T t) ;

    /**
     * Delete object by object id.
     * @param id technical identifier for object
     */
    void deleteById(K id) ;

    /**
     * Finds all objects.
     * @return list of objects
     */
    List<T> getAll() ;
}
