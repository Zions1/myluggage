package myLuggage.persist.daos.interfacesDAO;

import myLuggage.persist.model.ActivityEntity;

import java.util.List;

/**
 * Interface of Repository to handle activity.
 *
 * @author Wiktor Florencki
 */
public interface IActivityDAO extends IBaseDAO<ActivityEntity, Integer> {

    List<ActivityEntity>  getAllActivitiesWithoutBasic();
}
