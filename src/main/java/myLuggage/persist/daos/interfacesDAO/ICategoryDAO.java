package myLuggage.persist.daos.interfacesDAO;

import myLuggage.persist.model.CategoryEntity;

import java.util.List;

/**
 * Interface of Repository to handle category.
 *
 * @author Wiktor Florencki
 */
public interface ICategoryDAO extends IBaseDAO<CategoryEntity, Integer> {

}