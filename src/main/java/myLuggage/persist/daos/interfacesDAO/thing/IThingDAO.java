package myLuggage.persist.daos.interfacesDAO.thing;


import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.ThingEntity;

import java.util.List;

/**
 * Interface of Repository to handle thing.
 *
 * @author Wiktor Florencki
 */
public interface IThingDAO extends IBaseDAO<ThingEntity, Integer> {

}
