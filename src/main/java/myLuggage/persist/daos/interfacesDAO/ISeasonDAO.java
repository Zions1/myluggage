package myLuggage.persist.daos.interfacesDAO;

import myLuggage.persist.model.SeasonEntity;

/**
 * Interface of Repository to handle season.
 *
 * @author Wiktor Florencki
 */
public interface ISeasonDAO extends IBaseDAO<SeasonEntity, Integer> {

}
