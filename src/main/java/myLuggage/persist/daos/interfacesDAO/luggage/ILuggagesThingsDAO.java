package myLuggage.persist.daos.interfacesDAO.luggage;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.LuggagesThingsEntity;

import java.util.List;

/**
 * Interface of Repository to handle luggage thing.
 *
 * @author Wiktor Florencki
 */
public interface ILuggagesThingsDAO extends IBaseDAO<LuggagesThingsEntity, Integer> {

    void deleteLuggagesThings(int luggageId, int thingId);
}
