package myLuggage.persist.daos.interfacesDAO.luggage;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.LuggageEntity;

import java.util.List;

/**
 * Interface of Repository to handle luggage.
 *
 * @author Wiktor Florencki
 */
public interface ILuggageDAO extends IBaseDAO<LuggageEntity, Integer> {

    List<LuggageEntity> getAllUserLuggages(Integer id);
}
