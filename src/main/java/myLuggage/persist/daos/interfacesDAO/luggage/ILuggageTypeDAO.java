package myLuggage.persist.daos.interfacesDAO.luggage;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.LuggageTypeEntity;

import java.util.List;

/**
 * Interface of Repository to handle luggage type.
 *
 * @author Wiktor Florencki
 */
public interface ILuggageTypeDAO extends IBaseDAO<LuggageTypeEntity, Integer> {

}
