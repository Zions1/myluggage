package myLuggage.persist.daos.interfacesDAO.travel;


import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.TravelEntity;

import java.util.List;

/**
 * Interface of Repository to handle travel.
 *
 * @author Wiktor Florencki
 */
public interface ITravelDAO extends IBaseDAO<TravelEntity, Integer> {

    List<TravelEntity> getAllUserTravels(int id);

    Integer getLastId();
}
