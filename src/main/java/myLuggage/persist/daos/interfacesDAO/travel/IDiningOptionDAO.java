package myLuggage.persist.daos.interfacesDAO.travel;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.DiningOptionEntity;

import java.util.List;

/**
 * Interface of Repository to handle dining option.
 *
 * @author Wiktor Florencki
 */
public interface IDiningOptionDAO extends IBaseDAO<DiningOptionEntity, Integer> {

}
