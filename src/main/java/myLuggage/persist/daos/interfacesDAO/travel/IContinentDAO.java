package myLuggage.persist.daos.interfacesDAO.travel;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.ContinentEntity;

import java.util.List;

/**
 * Interface of Repository to handle continent.
 *
 * @author Wiktor Florencki
 */
public interface IContinentDAO extends IBaseDAO<ContinentEntity, Integer> {

}
