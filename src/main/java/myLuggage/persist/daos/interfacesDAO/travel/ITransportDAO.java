package myLuggage.persist.daos.interfacesDAO.travel;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.TransportEntity;

import java.util.List;

/**
 * Interface of Repository to handle transport.
 *
 * @author Wiktor Florencki
 */
public interface ITransportDAO extends IBaseDAO<TransportEntity, Integer> {

}
