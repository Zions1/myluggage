package myLuggage.persist.daos.interfacesDAO.travel;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.AccommodationEntity;
import java.util.List;

/**
 * Interface of Repository to handle accommodation.
 *
 * @author Wiktor Florencki
 */
public interface IAccommodationDAO extends IBaseDAO<AccommodationEntity, Integer> {

}
