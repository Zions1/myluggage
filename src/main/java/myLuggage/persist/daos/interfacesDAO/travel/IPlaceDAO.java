package myLuggage.persist.daos.interfacesDAO.travel;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.PlaceEntity;

import java.util.List;

/**
 * Interface of Repository to handle place.
 *
 * @author Wiktor Florencki
 */
public interface IPlaceDAO extends IBaseDAO<PlaceEntity, Integer> {

}
