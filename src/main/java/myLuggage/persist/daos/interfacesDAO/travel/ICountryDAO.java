package myLuggage.persist.daos.interfacesDAO.travel;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.CountryEntity;

import java.util.List;

/**
 * Interface of Repository to handle country.
 *
 * @author Wiktor Florencki
 */
public interface ICountryDAO extends IBaseDAO<CountryEntity, Integer> {

}
