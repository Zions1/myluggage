package myLuggage.persist.daos.interfacesDAO.user;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.UserEntity;

import java.sql.SQLException;
import java.util.List;

/**
 * Interface of Repository to handle user account.
 *
 * @author Wiktor Florencki
 */
public interface IUserDAO extends IBaseDAO<UserEntity, Integer> {

    UserEntity getUserByUsername (String username);

    UserEntity getUserByEmail(String email);

    void addUser(UserEntity user);

}
