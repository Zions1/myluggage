package myLuggage.persist.daos.interfacesDAO.user;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.UserAvatarEntity;


/**
 * Interface of Repository to handle user avatar.
 *
 * @author Wiktor Florencki
 */
public interface IUserAvatarDAO extends IBaseDAO<UserAvatarEntity, Integer> {

}
