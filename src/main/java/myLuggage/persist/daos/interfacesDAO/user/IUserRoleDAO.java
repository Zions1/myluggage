package myLuggage.persist.daos.interfacesDAO.user;

import myLuggage.persist.daos.interfacesDAO.IBaseDAO;
import myLuggage.persist.model.UserRoleEntity;

/**
 * Interface of Repository to handle role.
 *
 * @author Wiktor Florencki
 */
public interface IUserRoleDAO extends IBaseDAO<UserRoleEntity, Integer> {

    UserRoleEntity findByName(String name);
}
