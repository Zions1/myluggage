package myLuggage.services;


import myLuggage.persist.daos.implementationsDAO.travel.PlaceDAO;
import myLuggage.persist.daos.interfacesDAO.travel.IPlaceDAO;
import myLuggage.persist.dtos.PlaceDTO;
import myLuggage.persist.model.PlaceEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PlaceService implements IPlaceService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IPlaceDAO placeDAO;

    @Override
    public PlaceDTO getPlaceById(int id) {
        return placeDAO.getById(id) != null ? convertToDto(placeDAO.getById(id)) : null;
    }

    @Override
    public PlaceEntity getPlaceEntityById(int id) {
        return placeDAO.getById(id) != null ? placeDAO.getById(id) : null;
    }

    @Override
    public PlaceEntity getPlaceEntityByName(String name) {
        return placeDAO.findByName(name) != null ? placeDAO.findByName(name) : null;
    }

    @Override
    public List<PlaceDTO> getAllPlaces() {
        return placeDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public void addPlace(PlaceDTO Place) {
        placeDAO.create(convertToEntity(Place));
    }

    @Override
    public void updatePlace(PlaceDTO Place) {
        placeDAO.merge(convertToEntity(Place));
    }

    @Override
    public void deletePlace(int id) {
        placeDAO.deleteById(id);
    }

    private PlaceDTO convertToDto(PlaceEntity place) {
        return modelMapper.map(place, PlaceDTO.class);
    }

    private PlaceEntity convertToEntity(PlaceDTO place) {
        return modelMapper.map(place, PlaceEntity.class);
    }
}
