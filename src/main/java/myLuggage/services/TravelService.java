package myLuggage.services;

import myLuggage.persist.daos.implementationsDAO.*;
import myLuggage.persist.daos.implementationsDAO.travel.*;
import myLuggage.persist.daos.implementationsDAO.user.UserAvatarDAO;
import myLuggage.persist.daos.implementationsDAO.user.UserDAO;
import myLuggage.persist.daos.interfacesDAO.IActivityDAO;
import myLuggage.persist.daos.interfacesDAO.travel.IPlaceDAO;
import myLuggage.persist.daos.interfacesDAO.travel.ITravelDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserAvatarDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserDAO;
import myLuggage.persist.dtos.*;
import myLuggage.persist.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TravelService implements ITravelService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ITravelDAO travelDAO;

    @Autowired
    private IUserAvatarDAO avatarDAO;

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private IPlaceDAO placeDAO;

    @Autowired
    private IActivityDAO activityDAO;

    @Override
    public Integer addTravel(TravelDTO travel) {
        UserEntity user = travel.getUser();
        int avatarId = 0;
        if (user != null) {
            avatarId = user.getAvatar().getId();
            user.setAvatar(null);
            travel.setUser(user);
        }

        if (travel.getActivities() == null) {
            Collection collection = new ArrayList();
            travel.setActivities(collection);
        }
        Collection<ActivityEntity> activities = travel.getActivities();
        activities.add(activityDAO.findByName("Essentials"));
        activities.add(activityDAO.findByName("Toiletries"));
        activities.add(activityDAO.findByName("Free time"));
        travel.setActivities(activities);

        if (travel.getPlaces() != null) {
            for (PlaceEntity t : travel.getPlaces()) {
                t.setCountry(travel.getCountry());
                placeDAO.merge(t);
            }
        }
        travelDAO.merge(convertToEntity(travel));
        if(user != null){
            user.setAvatar(avatarDAO.getById(avatarId));
            userDAO.merge(user);
        }
        return travelDAO.getLastId();
    }


    @Override
    public TravelDTO getTravelById(int id) {
        return travelDAO.getById(id) != null ? convertToDto(travelDAO.getById(id)) : null;
    }

    @Override
    public TravelEntity getTravelEntityById(Integer id) {
        return travelDAO.getById(id) != null ? travelDAO.getById(id) : null;
    }

    @Override
    public List<TravelDTO> getAllUserTravels(String email) {
        return travelDAO.getAllUserTravels(userDAO.getUserByEmail(email).getId()).stream().map(this::convertToDto).collect(Collectors.toList());
    }


    @Override
    public void deleteTravel(Integer id) {
        travelDAO.deleteById(id);
    }

    @Override
    public void updateTravel(TravelDTO travel) {
        if (travel.getPlaces() != null) {
            for (PlaceEntity t : travel.getPlaces()) {
                t.setCountry(travel.getCountry());
                placeDAO.merge(t);
            }
        }
        travelDAO.update(convertToEntity(travel));
    }

    private TravelDTO convertToDto(TravelEntity travel) {
        return modelMapper.map(travel, TravelDTO.class);
    }

    private TravelEntity convertToEntity(TravelDTO travel) {
        return modelMapper.map(travel, TravelEntity.class);
    }
}
