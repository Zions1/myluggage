package myLuggage.services;

import myLuggage.persist.dtos.AccommodationDTO;
import myLuggage.persist.model.AccommodationEntity;

import java.util.List;

/**
 * Service that provides all method used by accommodation.
 *
 * @author Wiktor Florencki
 */
public interface IAccommodationService {

    /**
     * Get accommodation by id.
     * @param id technical identifier of accommodation
     * @return accommodation
     */
    AccommodationDTO getAccommodationById(int id);

    /**
     * Get accommodation entity by id.
     * @param id technical identifier of accommodation
     * @return entity of accommodation
     */
    AccommodationEntity getAccommodationEntityById(int id);

    /**
     * Gets all accommodations.
     * @return list of accommodation
     */
    List<AccommodationDTO> getAllAccommodations();

    /**
     * Create accommodation.
     * @param accommodation accommodation to create.
     */
    void addAccommodation(AccommodationDTO accommodation);

    /**
     * Update accommodation.
     * @param accommodation accommodation to update
     */
    void updateAccommodation(AccommodationDTO accommodation);

    /**
     * Delete accommodation.
     * @param id technical identifier of accommodation
     */
    void deleteAccommodation(int id);

}
