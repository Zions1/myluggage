package myLuggage.services;


import myLuggage.persist.daos.implementationsDAO.user.UserRoleDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserRoleDAO;
import myLuggage.persist.dtos.UserRoleDTO;
import myLuggage.persist.model.UserRoleEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserRoleService implements IUserRoleService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IUserRoleDAO roleDAO;

    @Override
    public UserRoleDTO getRoleById(int id) {
        return roleDAO.getById(id) != null ? convertToDto(roleDAO.getById(id)) : null;
    }

    @Override
    public UserRoleDTO getRoleByName(String name) {
        return roleDAO.findByName(name) != null ? convertToDto(roleDAO.findByName(name)) : null;
    }

    @Override
    public UserRoleEntity getRoleEntityByName(String name) {
        return roleDAO.findByName(name) != null ? roleDAO.findByName(name) : null;
    }

    @Override
    public List<UserRoleDTO> getAllRoles() {
        return roleDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    private UserRoleDTO convertToDto(UserRoleEntity role) {
        return modelMapper.map(role, UserRoleDTO.class);
    }
}
