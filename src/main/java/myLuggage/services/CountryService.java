package myLuggage.services;


import myLuggage.persist.daos.implementationsDAO.travel.CountryDAO;
import myLuggage.persist.daos.interfacesDAO.travel.ICountryDAO;
import myLuggage.persist.dtos.CountryDTO;
import myLuggage.persist.model.CountryEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CountryService implements ICountryService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ICountryDAO countryDAO;

    @Override
    public CountryEntity getCountryEntityById(int id) {
        return countryDAO.getById(id) != null ? countryDAO.getById(id) : null;
    }

    @Override
    public List<CountryDTO> getAllCountries() {
        return countryDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    private CountryDTO convertToDto(CountryEntity country) {
        return modelMapper.map(country, CountryDTO.class);
    }
}
