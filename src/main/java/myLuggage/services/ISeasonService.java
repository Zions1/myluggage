package myLuggage.services;

import myLuggage.persist.dtos.SeasonDTO;
import myLuggage.persist.model.SeasonEntity;

import java.util.List;

/**
 * Service that provides all method used by season.
 *
 * @author Wiktor Florencki
 */
public interface ISeasonService {

    /**
     * Get season by id.
     * @param id technical identifier of season
     * @return season
     */
    SeasonDTO getSeasonById(int id);

    /**
     * Get season entity by name.
     * @param name name of season
     * @return entity of season
     */
    SeasonEntity getSeasonEntityByName(String name);

    /**
     * Gets all seasons.
     * @return list of season
     */
    List<SeasonDTO> getAllSeasons();

    /**
     * Create season.
     * @param season season to create
     */
    void addSeason(SeasonDTO season);

    /**
     * Update season.
     * @param season season to update
     */
    void updateSeason(SeasonDTO season);

    /**
     * Delete season.
     * @param id technical identifier of season
     */
    void deleteSeason(int id);

}
