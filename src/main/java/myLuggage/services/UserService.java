package myLuggage.services;

import myLuggage.persist.daos.implementationsDAO.user.UserAvatarDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserAvatarDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserRoleDAO;
import myLuggage.persist.dtos.UserAvatarDTO;
import myLuggage.persist.model.UserAvatarEntity;
import myLuggage.persist.model.UserRoleEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import myLuggage.persist.daos.implementationsDAO.user.UserRoleDAO;
import myLuggage.persist.daos.implementationsDAO.user.UserDAO;
import myLuggage.persist.dtos.UserDTO;
import myLuggage.persist.model.UserEntity;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService implements IUserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IUserAvatarDAO avatarDAO;

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private IUserRoleDAO roleDAO;


    @Override
    public UserDTO getUserById(int id) {
        return convertToDto(userDAO.getById(id));
    }

    @Override
    public UserDTO getUserByEmail(String email) {
        return userDAO.getUserByEmail(email) != null ? convertToDto(userDAO.getUserByEmail(email)) : null;
    }

    @Override
    public UserEntity getUserEntityByEmail(String email) {
        return userDAO.getUserByEmail(email) != null ? userDAO.getUserByEmail(email) : null;
    }

    @Override
    public UserDTO getUserByUsername(String name) {
        return userDAO.getUserByEmail(name) != null ? convertToDto(userDAO.getUserByEmail(name)) : null;
    }

    @Override
    public void addUser(UserDTO userDTO) {
        UserRoleEntity userRole = roleDAO.findByName("ROLE_USER");
        userDTO.setRoles(Arrays.asList(userRole));
        userDTO.setActive(Boolean.FALSE);
        userDTO.setAvatar(avatarDAO.getById(1));
        userDAO.addUser(convertToEntity(userDTO));
    }

    @Override
    public void activateUser(UserDTO userDTO) {
        userDTO.setActive(Boolean.TRUE);
        userDAO.merge(convertToEntity(userDTO));
    }

    @Override
    public void updateUser(Integer userId, UserDTO user) {
        user.setTravels(this.getUserById(user.getId()).getTravels());
        user.setAvatar(this.getUserById(user.getId()).getAvatar());
        if(user.getRoles() == null) {
            user.setRoles(Arrays.asList(roleDAO.findByName("ROLE_USER")));
        }else{
            user.getRoles().add(roleDAO.findByName("ROLE_USER"));
        }
        userDAO.merge(convertToEntity(user));
    }

    @Override
    public void updateUserDetails(UserDTO userDTO) {
        UserDTO user = convertToDto(userDAO.getById(userDTO.getId()));
        user.setName(userDTO.getName());
        user.setLastname(userDTO.getLastname());
        user.setGender(userDTO.getGender());
        user.setBirthDate(userDTO.getBirthDate());
        userDAO.merge(convertToEntity(user));
    }

    @Override
    public void updateUserAvatar(UserDTO userDTO, UserAvatarDTO avatarDTO) {
        userDTO.setAvatar(convertToEntity(avatarDTO));
        userDAO.update(convertToEntity(userDTO));
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public byte[] getAvatar(int id) throws SQLException {
        UserEntity user = userDAO.getById(id);
        return user.getAvatar().getAvatar().getBytes(1, Long.valueOf(user.getAvatar().getAvatar().length()).intValue());
    }

    @Override
    public void changePassword(int id, String oldPassword, String newPassword) {
        UserDTO user = convertToDto(userDAO.getById(id));
        if (passwordEncoder.matches(oldPassword, user.getPassword())) {
            user.setPassword(passwordEncoder.encode(newPassword));
            userDAO.merge(convertToEntity(user));
        }
    }

    @Override
    public void changePassword(int id, String newPassword) {
        UserDTO user = convertToDto(userDAO.getById(id));
        user.setPassword(passwordEncoder.encode(newPassword));
        userDAO.merge(convertToEntity(user));
    }

    @Override
    public void deleteUser(int id) {
        userDAO.deleteById(id);
    }


    private UserDTO convertToDto(UserEntity user) {
        return modelMapper.map(user, UserDTO.class);
    }

    private UserEntity convertToEntity(UserDTO user) {
        return user != null ? modelMapper.map(user, UserEntity.class) : null;
    }

    private UserAvatarEntity convertToEntity(UserAvatarDTO avatar) {
        return modelMapper.map(avatar, UserAvatarEntity.class);
    }
}
