package myLuggage.services;

import myLuggage.persist.dtos.LuggageDTO;
import myLuggage.persist.dtos.LuggagesThingsDTO;
import myLuggage.persist.model.LuggageEntity;

import java.util.List;

/**
 * Service that provides all method used by luggage.
 *
 * @author Wiktor Florencki
 */
public interface ILuggageService {

    /**
     * Get luggage by id.
     * @param id technical identifier of luggage
     * @return luggage
     */
    LuggageDTO getLuggageById(int id);

    /**
     * Get luggage entity by id.
     * @param id technical identifier of luggage
     * @return entity of luggage
     */
    LuggageEntity getLuggageEntityById(int id);

    /**
     * Gets all luggage's.
     * @return list of luggage
     */
    List<LuggageDTO> getAllLuggages();

    /**
     * Gets all user luggage's.
     * @return list of luggage
     */
    List<LuggageDTO> getAllUserLuggages(String email);

    /**
     * Create luggage.
     * @param luggage luggage
     */
    void addLuggage(LuggageDTO luggage);

    /**
     * Create luggage things.
     * @param luggage luggage to create
     */
    void addLuggageThings(LuggageDTO luggage);

    /**
     * Update luggage.
     * @param luggage luggage to update
     */
    void updateLuggage(LuggageDTO luggage);

    /**
     * Update luggage things.
     * @param luggageThing luggage thing
     * @param luggageId technical identifier of luggage
     */
    void updateLuggageThing(LuggagesThingsDTO luggageThing, int luggageId);

    /**
     * Update luggage name.
     * @param luggageId technical identifier of luggage
     * @param name new name
     */
    void updateLuggageName(int luggageId, String name);

    /**
     * Delete luggage.
     * @param id technical identifier of luggage
     */
    void deleteLuggage(int id);

    /**
     * Delete thing from luggage.
     * @param luggageId technical identifier of luggage
     * @param thingId technical identifier of thing
     */
    void deleteLuggageThing(int luggageId, int thingId);
}
