package myLuggage.services;

import myLuggage.persist.daos.interfacesDAO.ISeasonDAO;
import myLuggage.persist.dtos.SeasonDTO;
import myLuggage.persist.model.SeasonEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class SeasonService implements ISeasonService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ISeasonDAO seasonDAO;

      @Override
    public SeasonDTO getSeasonById(int id) {
        return seasonDAO.getById(id) != null ? convertToDto(seasonDAO.getById(id)) : null;
    }

    @Override
    public SeasonEntity getSeasonEntityByName(String name) {
        return seasonDAO.findByName(name) != null ? seasonDAO.findByName(name) : null;
    }

    @Override
    public List<SeasonDTO> getAllSeasons() {
        return seasonDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public void addSeason(SeasonDTO season) {
        seasonDAO.create(convertToEntity(season));
    }

    @Override
    public void updateSeason(SeasonDTO season) {
        seasonDAO.merge(convertToEntity(season));
    }

    @Override
    public void deleteSeason(int id) {
        seasonDAO.deleteById(id);
    }

    private SeasonDTO convertToDto(SeasonEntity season) {
        return modelMapper.map(season, SeasonDTO.class);
    }

    private SeasonEntity convertToEntity(SeasonDTO season) {
        return modelMapper.map(season, SeasonEntity.class);
    }
}
