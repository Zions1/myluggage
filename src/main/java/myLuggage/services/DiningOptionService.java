package myLuggage.services;


import myLuggage.persist.daos.implementationsDAO.travel.DiningOptionDAO;
import myLuggage.persist.daos.interfacesDAO.travel.IDiningOptionDAO;
import myLuggage.persist.dtos.DiningOptionDTO;
import myLuggage.persist.model.DiningOptionEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class DiningOptionService implements IDiningOptionService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IDiningOptionDAO diningOptionDAO;

    @Override
    public DiningOptionDTO getDiningOptionById(int id) {
        return diningOptionDAO.getById(id) != null ? convertToDto(diningOptionDAO.getById(id)) : null;
    }

    @Override
    public DiningOptionEntity getDiningOptionEntityById(int id) {
        return diningOptionDAO.getById(id) != null ? diningOptionDAO.getById(id) : null;
    }

    @Override
    public List<DiningOptionDTO> getAllDiningOptions() {
        return diningOptionDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public void addDiningOption(DiningOptionDTO DiningOption) {
        diningOptionDAO.create(convertToEntity(DiningOption));
    }

    @Override
    public void updateDiningOption(DiningOptionDTO DiningOption) {
        diningOptionDAO.merge(convertToEntity(DiningOption));
    }

    @Override
    public void deleteDiningOption(int id) {
        diningOptionDAO.deleteById(id);
    }

    private DiningOptionDTO convertToDto(DiningOptionEntity diningOptionDAO) {
        return modelMapper.map(diningOptionDAO, DiningOptionDTO.class);
    }

    private DiningOptionEntity convertToEntity(DiningOptionDTO diningOptionDAO) {
        return modelMapper.map(diningOptionDAO, DiningOptionEntity.class);
    }
}
