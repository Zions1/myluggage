package myLuggage.services;

import myLuggage.persist.dtos.UserRoleDTO;
import myLuggage.persist.model.UserRoleEntity;

import java.util.List;

/**
 * Service that provides all method used to manage roles.
 *
 * @author Wiktor Florencki
 */
public interface IUserRoleService {

    /**
     * Find role by technical identifier.
     *
     * @param id technical identifier of a role
     * @return role of a user
     */
    UserRoleDTO getRoleById(int id);

    /**
     * Find role by name.
     *
     * @param name name of a role
     * @return role of a user
     */
    UserRoleDTO getRoleByName(String name);

    /**
     * Find role by role name.
     *
     * @param name name of a role
     * @return user role entity
     */
    UserRoleEntity getRoleEntityByName(String name);

    /**
     * Finds all roles.
     *
     * @return list of roles
     */
    List<UserRoleDTO> getAllRoles();
}