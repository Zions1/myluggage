package myLuggage.services;

import myLuggage.persist.dtos.CountryDTO;
import myLuggage.persist.model.CountryEntity;

import java.util.List;

/**
 * Service that provides all method used by country.
 *
 * @author Wiktor Florencki
 */
public interface ICountryService {

    /**
     * Get country entity by id.
     * @param id technical identifier of country
     * @return entity of country
     */
    CountryEntity getCountryEntityById(int id);

    /**
     * Gets all countries.
     * @return list of country
     */
    List<CountryDTO> getAllCountries();
}
