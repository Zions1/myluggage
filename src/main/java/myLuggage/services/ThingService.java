package myLuggage.services;

import myLuggage.persist.daos.implementationsDAO.thing.ThingDAO;
import myLuggage.persist.daos.interfacesDAO.thing.IThingDAO;
import myLuggage.persist.dtos.ThingDTO;
import myLuggage.persist.model.LuggagesThingsEntity;
import myLuggage.persist.model.ThingEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ThingService implements IThingService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IThingDAO thingDAO;


    @Override
    public ThingDTO getThingById(int id) {
        return thingDAO.getById(id) != null ? convertToDto(thingDAO.getById(id)) : null;
    }

    @Override
    public List<ThingDTO> getAllThings() {
        return thingDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public List<ThingDTO> getThingsWithoutSelected(Collection<LuggagesThingsEntity> things) {
        List<ThingDTO> list = this.getAllThings();
        for (ThingDTO t1 : this.getAllThings()) {
            for (LuggagesThingsEntity t2 : things) {
                if (t1.getId().equals(t2.getThing().getId())) {
                    list.remove(t1);
                }
            }
        }
        return list;
    }

    @Override
    public void addThing(ThingDTO thing) {
        thingDAO.create(convertToEntity(thing));
    }

    @Override
    public void addThingEntity(ThingEntity thing) {
        thingDAO.create(thing);
    }

    @Override
    public void updateThing(ThingDTO thing) {
        thingDAO.merge(convertToEntity(thing));
    }

    @Override
    public void deleteThing(int id) {
        thingDAO.deleteById(id);
    }

    @Override
    public ThingEntity getThingEntityById(int id) {
        return thingDAO.getById(id) != null ? thingDAO.getById(id) : null;
    }

    @Override
    public boolean thingExist(ThingEntity thing) {
        for (ThingEntity t : thingDAO.getAll()) {
            if (t.getName().toLowerCase().equals(thing.getName().toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    private ThingDTO convertToDto(ThingEntity thing) {
        return modelMapper.map(thing, ThingDTO.class);
    }

    private ThingEntity convertToEntity(ThingDTO thing) {
        return modelMapper.map(thing, ThingEntity.class);
    }
}
