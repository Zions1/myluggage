package myLuggage.services;

import myLuggage.persist.dtos.LuggageTypeDTO;
import myLuggage.persist.model.LuggageTypeEntity;

import java.util.List;

/**
 * Service that provides all method used by luggage type.
 *
 * @author Wiktor Florencki
 */
public interface ILuggageTypeService {

    /**
     * Get luggage type by id.
     * @param id technical identifier of luggage type
     * @return luggage type
     */
    LuggageTypeDTO getLuggageTypeById(int id);

    /**
     * Get luggage type entity by name.
     * @param name name of luggage type
     * @return entity of luggage type
     */
    LuggageTypeEntity getLuggageTypeEntityByName(String name);

    /**
     * Gets all luggage types.
     * @return list of luggage type
     */
    List<LuggageTypeDTO> getAllLuggageTypes();

    /**
     * Create luggage type.
     * @param luggageType type luggage type
     */
    void addLuggageType(LuggageTypeDTO luggageType);

    /**
     * Update luggage type.
     * @param luggageType type luggage type to update
     */ 
    void updateLuggageType(LuggageTypeDTO luggageType);
    
    /**
     * Delete luggage type.
     * @param id technical identifier of luggage type
     */
    void deleteLuggageType(int id);
}
