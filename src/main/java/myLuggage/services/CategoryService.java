package myLuggage.services;

import myLuggage.persist.daos.implementationsDAO.CategoryDAO;
import myLuggage.persist.daos.interfacesDAO.ICategoryDAO;
import myLuggage.persist.dtos.CategoryDTO;
import myLuggage.persist.model.CategoryEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryService implements ICategoryService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ICategoryDAO categoryDAO;

    @Override
    public CategoryDTO getCategoryById(int id) {
        return convertToDto(this.getCategoryEntityById(id));
    }

    @Override
    public CategoryDTO getCategoryByName(String name) {
        return categoryDAO.findByName(name) != null ? convertToDto(categoryDAO.findByName(name)) : null;
    }

    @Override
    public CategoryEntity getCategoryEntityById(int id) {
        return categoryDAO.getById(id) != null ? categoryDAO.getById(id) : null;
    }

    @Override
    public CategoryEntity getCategoryEntityByName(String name) {
        return categoryDAO.findByName(name) != null ? categoryDAO.findByName(name) : null;
    }

    @Override
    public List<CategoryDTO> getAllCategories() {
        return categoryDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public void addCategory(CategoryDTO Category) {
        categoryDAO.create(convertToEntity(Category));
    }

    @Override
    public void updateCategory(CategoryDTO Category) {
        categoryDAO.merge(convertToEntity(Category));
    }

    @Override
    public void deleteCategory(int id) {
        categoryDAO.deleteById(id);
    }

    private CategoryDTO convertToDto(CategoryEntity category) {
        return modelMapper.map(category, CategoryDTO.class);
    }

    private CategoryEntity convertToEntity(CategoryDTO category) {
        return modelMapper.map(category, CategoryEntity.class);
    }
}
