package myLuggage.services;


import myLuggage.persist.dtos.UserDTO;

/**
 * Service that provides all method used by email.
 *
 * @author Wiktor Florencki
 */
public interface IEmailService {

    /**
     * Sends confirmation of registration message.
     * @param user user data
     */
    void confirmRegistration(UserDTO user);

    /**
     * Sends reset password message, containing a new password.
     * @param user user data
     * @param newPassword new password
     */
    void resetPassword(UserDTO user, String newPassword);
}
