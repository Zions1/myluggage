package myLuggage.services;

import myLuggage.persist.dtos.ActivityDTO;
import myLuggage.persist.model.ActivityEntity;

import java.util.Collection;
import java.util.List;

/**
 * Service that provides all method used by activity.
 *
 * @author Wiktor Florencki
 */
public interface IActivityService {

    /**
     * Get activity by id.
     * @param id technical identifier of activity
     * @return activity
     */
    ActivityDTO getActivityById(int id);

    /**
     * Get activity entity by id.
     * @param id technical identifier of activity
     * @return entity of activity
     */
    ActivityEntity getActivityEntityById(int id);

    /**
     * Get activity entity by name.
     * @param name name of activity
     * @return entity of activity
     */
    ActivityEntity getActivityEntityByName(String name);

    /**
     * Gets all activities.
     * @return list of activity
     */
    List<ActivityDTO> getAllActivities();

    /**
     * Gets activities list without Essentials, Toiletries and Free time.
     * @return list of activity
     */
    List<ActivityDTO> getAllActivitiesWithoutBasic();

    /**
     * Gets activities list without selected.
     * @return list of activity
     */
    List<ActivityDTO> getActivitiesWithoutSelected(Collection<ActivityEntity> activities);

    /**
     * Create activity.
     * @param activity activity
     */
    void addActivity(ActivityDTO activity);

    /**
     * Update activity.
     * @param activity activity to update
     */
    void updateActivity(ActivityDTO activity);

    /**
     * Delete activity.
     * @param id technical identifier of activity
     */
    void deleteActivity(int id);
}
