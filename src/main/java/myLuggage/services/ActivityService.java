package myLuggage.services;


import myLuggage.persist.daos.implementationsDAO.ActivityDAO;
import myLuggage.persist.daos.interfacesDAO.IActivityDAO;
import myLuggage.persist.dtos.ActivityDTO;
import myLuggage.persist.model.ActivityEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ActivityService implements IActivityService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IActivityDAO activityDAO;

    @Override
    public ActivityDTO getActivityById(int id) {
        return convertToDto(this.getActivityEntityById(id));
    }
    
    @Override
    public ActivityEntity getActivityEntityById(int id) {
        return activityDAO.getById(id) != null ? activityDAO.getById(id) : null;
    }
    @Override
    public List<ActivityDTO> getAllActivities() {
        return activityDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }
    
    @Override
    public List<ActivityDTO> getAllActivitiesWithoutBasic() {
       return activityDAO.getAllActivitiesWithoutBasic().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public List<ActivityDTO> getActivitiesWithoutSelected(Collection<ActivityEntity> activities) {
        List<ActivityDTO> list = this.getAllActivities();
        for(ActivityDTO t1: this.getAllActivities()){
            for(ActivityEntity t2: activities){
                if(t1.getId().equals(t2.getId())){
                    list.remove(t1);
                }
            }
        }
        return list;
    }

    @Override
    public void addActivity(ActivityDTO activity) {
        activityDAO.create(convertToEntity(activity));
    }

    @Override
    public void updateActivity(ActivityDTO activity) {
        activityDAO.merge(convertToEntity(activity));
    }

    @Override
    public void deleteActivity(int id) {
        activityDAO.deleteById(id);
    }


    @Override
    public ActivityEntity getActivityEntityByName(String name) {
        return activityDAO.findByName(name) != null ? activityDAO.findByName(name) : null;
    }

    private ActivityDTO convertToDto(ActivityEntity activity) {
        return modelMapper.map(activity, ActivityDTO.class);
    }

    private ActivityEntity convertToEntity(ActivityDTO activity) {
        return modelMapper.map(activity, ActivityEntity.class);
    }
}
