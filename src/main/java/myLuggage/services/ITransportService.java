package myLuggage.services;

import myLuggage.persist.dtos.TransportDTO;
import myLuggage.persist.model.TransportEntity;

import java.util.Collection;
import java.util.List;

/**
 * Service that provides all method used by transport.
 *
 * @author Wiktor Florencki
 */
public interface ITransportService {

    /**
     * Get transport by id.
     * @param id technical identifier of transport
     * @return transport
     */
    TransportDTO getTransportById(int id);

    /**
     * Get transport entity by id.
     * @param id technical identifier of transport
     * @return entity of transport
     */
    TransportEntity getTransportEntityById(int id);

    /**
     * Gets all transports.
     * @return list of transport
     */
    List<TransportDTO> getAllTransports();

    /**
     * Gets all transports.
     * @return list of transport
     */
    List<TransportDTO> getTransportsWithoutSelected(Collection<TransportEntity> transports);

    /**
     * Create transport.
     * @param transport transport
     */
    void addTransport(TransportDTO transport);

    /**
     * Update transport.
     * @param transport transport to update
     */
    void updateTransport(TransportDTO transport);
    /**
     * Delete transport.
     * @param id technical identifier of transport
     */
    void deleteTransport(int id);

}
