package myLuggage.services;


import myLuggage.persist.daos.interfacesDAO.travel.IAccommodationDAO;
import myLuggage.persist.dtos.AccommodationDTO;
import myLuggage.persist.model.AccommodationEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccommodationService implements IAccommodationService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IAccommodationDAO accommodationDAO;

    @Override
    public AccommodationDTO getAccommodationById(int id) {
        return accommodationDAO.getById(id) != null ? convertToDto(accommodationDAO.getById(id)) : null;
    }

    @Override
    public AccommodationEntity getAccommodationEntityById(int id) {
        return accommodationDAO.getById(id) != null ? accommodationDAO.getById(id) : null;
    }

    @Override
    public List<AccommodationDTO> getAllAccommodations() {
        return accommodationDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public void addAccommodation(AccommodationDTO Accommodation) {
        accommodationDAO.merge(convertToEntity(Accommodation));
    }

    @Override
    public void updateAccommodation(AccommodationDTO Accommodation) {
        accommodationDAO.create(convertToEntity(Accommodation));
    }

    @Override
    public void deleteAccommodation(int id) {
        accommodationDAO.deleteById(id);
    }

    private AccommodationDTO convertToDto(AccommodationEntity accommodation) {
        return modelMapper.map(accommodation, AccommodationDTO.class);
    }

    private AccommodationEntity convertToEntity(AccommodationDTO accommodation) {
        return modelMapper.map(accommodation, AccommodationEntity.class);
    }
}
