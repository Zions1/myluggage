package myLuggage.services;


import myLuggage.persist.daos.implementationsDAO.travel.TransportDAO;
import myLuggage.persist.daos.interfacesDAO.travel.ITransportDAO;
import myLuggage.persist.dtos.TransportDTO;
import myLuggage.persist.model.TransportEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransportService implements ITransportService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ITransportDAO transportDAO;

    @Override
    public TransportDTO getTransportById(int id) {
        return transportDAO.getById(id) != null ? convertToDto(transportDAO.getById(id)) : null;
    }
    @Override
    public TransportEntity getTransportEntityById(int id) {
        return transportDAO.getById(id) != null ? transportDAO.getById(id) : null;
    }

    @Override
    public List<TransportDTO> getAllTransports() {
        return transportDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public List<TransportDTO> getTransportsWithoutSelected(Collection<TransportEntity> transports) {
        List<TransportDTO> list = this.getAllTransports();
        for(TransportDTO t1: this.getAllTransports()){
            for(TransportEntity t2: transports){
                if(t1.getId().equals(t2.getId())){
                    list.remove(t1);
                }
            }
        }
        return list;
    }

    @Override
    public void addTransport(TransportDTO transport) {
        transportDAO.create(convertToEntity(transport));
    }

    @Override
    public void updateTransport(TransportDTO transport) {
        transportDAO.merge(convertToEntity(transport));
    }

    @Override
    public void deleteTransport(int id) {
        transportDAO.deleteById(id);
    }

    private TransportDTO convertToDto(TransportEntity transport) {
        return modelMapper.map(transport, TransportDTO.class);
    }

    private TransportEntity convertToEntity(TransportDTO transport) {
        return modelMapper.map(transport, TransportEntity.class);
    }
}
