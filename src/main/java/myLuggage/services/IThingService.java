package myLuggage.services;

import myLuggage.persist.dtos.ThingDTO;
import myLuggage.persist.model.LuggagesThingsEntity;
import myLuggage.persist.model.ThingEntity;

import java.util.Collection;
import java.util.List;

public interface IThingService {

    /**
     * Get thing by id.
     * @param id technical identifier of thing
     * @return thing
     */
    ThingDTO getThingById(int id);

    /**
     * Get thing entity by id.
     * @param id technical identifier of thing
     * @return entity of thing
     */
    ThingEntity getThingEntityById(int id);

    /**
     * Gets all things.
     * @return list of thing
     */
    List<ThingDTO> getAllThings();

    /**
     * Gets all unselected things.
     * @return list of thing
     */
    List<ThingDTO> getThingsWithoutSelected(Collection<LuggagesThingsEntity> things);

    /**
     * Create thing.
     * @param thing thing to create
     */
    void addThing(ThingDTO thing);

    /**
     * Create thing.
     * @param thing thing to create
     */
    void addThingEntity(ThingEntity thing);

    /**
     * Update thing.
     * @param thing thing to update
     */
    void updateThing(ThingDTO thing);

    /**
     * Delete thing.
     * @param id technical identifier of thing
     */
    void deleteThing(int id);

    /**
     * Check if thing exist.
     * @param thing thing
     * @return true if thing exist or false if not
     */
    boolean thingExist(ThingEntity thing);
}
