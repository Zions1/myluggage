package myLuggage.services;

import myLuggage.persist.dtos.PlaceDTO;
import myLuggage.persist.model.PlaceEntity;

import java.util.List;

/**
 * Service that provides all method used by place.
 *
 * @author Wiktor Florencki
 */
public interface IPlaceService {

    /**
     * Get place by id.
     * @param id technical identifier of place
     * @return place
     */
    PlaceDTO getPlaceById(int id);

    /**
     * Get place entity by id.
     * @param id technical identifier of place
     * @return entity of place
     */
    PlaceEntity getPlaceEntityById(int id);

    /**
     * Get place entity by name
     * @param name name of place
     * @return entity of place
     */
    PlaceEntity getPlaceEntityByName(String name);
    
    /**
     * Gets all places.
     * @return list of place
     */
    List<PlaceDTO> getAllPlaces();

    /**
     * Create place.
     * @param place place to create.
     */
    void addPlace(PlaceDTO place);
    
    /**
     * Update place.
     * @param place place to update
     */
    void updatePlace(PlaceDTO place);

    /**
     * Delete place.
     * @param id technical identifier of place
     */
    void deletePlace(int id);

}
