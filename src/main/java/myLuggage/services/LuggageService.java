package myLuggage.services;

import myLuggage.persist.daos.implementationsDAO.luggage.LuggageDAO;
import myLuggage.persist.daos.implementationsDAO.luggage.LuggagesThingsThingsDAO;
import myLuggage.persist.daos.implementationsDAO.thing.ThingDAO;
import myLuggage.persist.daos.implementationsDAO.user.UserDAO;
import myLuggage.persist.daos.interfacesDAO.luggage.ILuggageDAO;
import myLuggage.persist.daos.interfacesDAO.thing.IThingDAO;
import myLuggage.persist.daos.interfacesDAO.user.IUserDAO;
import myLuggage.persist.dtos.LuggageDTO;
import myLuggage.persist.dtos.LuggagesThingsDTO;
import myLuggage.persist.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class LuggageService implements ILuggageService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private ILuggageDAO luggageDAO;

    @Autowired
    private IThingDAO thingDAO;

    @Autowired
    private LuggagesThingsThingsDAO luggagesThingsThingsDAO;

    @Override
    public LuggageDTO getLuggageById(int id) {
        return luggageDAO.getById(id) != null ? convertToDto(luggageDAO.getById(id)) : null;
    }

    @Override
    public LuggageEntity getLuggageEntityById(int id) {
        return luggageDAO.getById(id) != null ? luggageDAO.getById(id) : null;
    }

    @Override
    public List<LuggageDTO> getAllLuggages() {
        return luggageDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public List<LuggageDTO> getAllUserLuggages(String email) {
        return luggageDAO.getAllUserLuggages(userDAO.getUserByEmail(email).getId()).stream().map(this::convertToDto).collect(Collectors.toList());

    }

    @Override
    public void addLuggage(LuggageDTO luggage) {
        luggageDAO.create(convertToEntity(luggage));
    }

    @Override
    public void addLuggageThings(LuggageDTO luggage) {
        if (luggage.getThings() != null) {
            LuggageEntity luggageEntity = convertToEntity(luggage);
            for (LuggagesThingsEntity l : luggage.getThings()) {
                l.setLuggage(luggageEntity);
                luggagesThingsThingsDAO.update(l);
            }
            luggageDAO.merge(luggageEntity);
        } else {
            luggage.setThings(null);
        }
    }


    @Override
    public void updateLuggage(LuggageDTO luggage) {
        LuggageDTO l = this.getLuggageById(luggage.getId());
        l.setName(luggage.getName());
        l.setCategory(luggage.getCategory());
        l.setType(luggage.getType());
        luggageDAO.merge(convertToEntity(l));
    }

    @Override
    public void deleteLuggage(int id) {
        luggageDAO.deleteById(id);
    }

    @Override
    public void deleteLuggageThing(int luggageId, int thingId) {
        luggagesThingsThingsDAO.deleteLuggagesThings(luggageId, thingId);
    }

    @Override
    public void updateLuggageThing(LuggagesThingsDTO luggageThing, int luggageId) {
        luggageThing.setThing(thingDAO.findByName(luggageThing.getThing().getName()));
        luggageThing.setLuggage(this.getLuggageEntityById(luggageId));
        luggagesThingsThingsDAO.update(convertToEntity(luggageThing));
    }

    @Override
    public void updateLuggageName(int luggageId, String name) {
        LuggageDTO luggage = this.getLuggageById(luggageId);
        luggage.setName(name);
        luggageDAO.merge(convertToEntity(luggage));
    }

    private LuggageDTO convertToDto(LuggageEntity luggage) {
        return modelMapper.map(luggage, LuggageDTO.class);
    }

    private LuggageEntity convertToEntity(LuggageDTO luggage) {
        return modelMapper.map(luggage, LuggageEntity.class);
    }

    private LuggagesThingsEntity convertToEntity(LuggagesThingsDTO luggage) {
        return modelMapper.map(luggage, LuggagesThingsEntity.class);
    }
}
