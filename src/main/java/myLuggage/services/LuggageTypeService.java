package myLuggage.services;


import myLuggage.persist.daos.implementationsDAO.luggage.LuggageTypeDAO;
import myLuggage.persist.daos.interfacesDAO.luggage.ILuggageTypeDAO;
import myLuggage.persist.dtos.LuggageTypeDTO;
import myLuggage.persist.model.LuggageTypeEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class LuggageTypeService implements ILuggageTypeService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ILuggageTypeDAO luggageTypeDAO;

    @Override
    public LuggageTypeDTO getLuggageTypeById(int id) {
        return luggageTypeDAO.getById(id) != null ? convertToDto(luggageTypeDAO.getById(id)) : null;
    }

    @Override
    public LuggageTypeEntity getLuggageTypeEntityByName(String name) {
        return luggageTypeDAO.findByName(name) != null ? luggageTypeDAO.findByName(name) : null;
    }

    @Override
    public List<LuggageTypeDTO> getAllLuggageTypes() {
        return luggageTypeDAO.getAll().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public void addLuggageType(LuggageTypeDTO LuggageType) {
        luggageTypeDAO.create(convertToEntity(LuggageType));
    }

    @Override
    public void updateLuggageType(LuggageTypeDTO LuggageType) {
        luggageTypeDAO.merge(convertToEntity(LuggageType));
    }

    @Override
    public void deleteLuggageType(int id) {
        luggageTypeDAO.deleteById(id);
    }

    private LuggageTypeDTO convertToDto(LuggageTypeEntity luggageType) {
        return modelMapper.map(luggageType, LuggageTypeDTO.class);
    }

    private LuggageTypeEntity convertToEntity(LuggageTypeDTO luggageType) {
        return modelMapper.map(luggageType, LuggageTypeEntity.class);
    }
}
