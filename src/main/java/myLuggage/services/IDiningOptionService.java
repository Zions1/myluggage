package myLuggage.services;

import myLuggage.persist.dtos.DiningOptionDTO;
import myLuggage.persist.model.DiningOptionEntity;

import java.util.List;

/**
 * Service that provides all method used by dining option.
 *
 * @author Wiktor Florencki
 */
public interface IDiningOptionService {

    /**
     * Get dining option by id.
     * @param id technical identifier of dining option
     * @return dining option
     */
    DiningOptionDTO getDiningOptionById(int id);

    /**
    * Get dining option entity by id.
    * @param id technical identifier of dining option
    * @return entity of dining option
    */
    DiningOptionEntity getDiningOptionEntityById(int id);

    /**
     * Gets all dining options.
     * @return list of dining option
     */
    List<DiningOptionDTO> getAllDiningOptions();

    /**
     * Create dining option.
     * @param diningOption dining option
     */
    void addDiningOption(DiningOptionDTO diningOption);

    /**
     * Update dining option.
     * @param diningOption dining option to update
     */
    void updateDiningOption(DiningOptionDTO diningOption);

    /**
     * Delete dining option.
     * @param id technical identifier of dining option
     */
    void deleteDiningOption(int id);

}