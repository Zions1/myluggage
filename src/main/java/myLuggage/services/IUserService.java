package myLuggage.services;

import myLuggage.persist.dtos.UserAvatarDTO;
import myLuggage.persist.dtos.UserDTO;
import myLuggage.persist.model.UserEntity;

import java.sql.SQLException;
import java.util.List;

/**
 * Service that provides all method used by user.
 *
 * @author Wiktor Florencki
 */
public interface IUserService {

    /**
     * Get user by id.
     * @param id technical identifier of a user
     * @return user
     */
    UserDTO getUserById(int id);

    /**
     * Get user by email.
     * @param email email of the user
     * @return user
     */
    UserDTO getUserByEmail(String email);

    /**
     * Get user entity by email.
     * @param email email of a user
     * @return entity of the user
     */
    UserEntity getUserEntityByEmail(String email);

    /**
     * Get user by name.
     * @param name name of the user
     * @return user
     */
    UserDTO getUserByUsername(String name);

    /**
     * Create the user.
     * @param userDTO user
     */
    void addUser(UserDTO userDTO);

    /**
     * Activate the user.
     * @param userDTO user
     */
    void activateUser(UserDTO userDTO);

    /**
     * Update the user.
     * @param userId technical identifier of the user
     * @param userDTO user
     */
    void updateUser(Integer userId, UserDTO userDTO);

    /**
     * Update user details.
     * @param userDTO user
     */
    void updateUserDetails(UserDTO userDTO);

    /**
     * Update user avatar.
     * @param userDTO user
     * @param userAvatarDTO avatar of the user
     */
    void updateUserAvatar(UserDTO userDTO, UserAvatarDTO userAvatarDTO);

    /**
     * Gets all users.
     * @return list of users.
     */
    List<UserDTO> getAllUsers();

    /**
     * Get user avatar.
     * @param id technical identifier of the user
     * @return avatar of the user
     * @throws SQLException exception
     */
    byte[] getAvatar(int id) throws SQLException;

    /**
     * Change user password.
     * @param id technical identifier of the user
     * @param oldPassword old password of the user
     * @param newPassword new password of the user
     */
    void changePassword(int id, String oldPassword, String newPassword);

    /**
     * Change user password.
     * @param id technical identifier of the user
     * @param newPassword new password of the user
     */
    void changePassword(int id, String newPassword);

    /**
     * Delete the user.
     * @param id technical identifier of the user
     */
    void deleteUser(int id);

}
