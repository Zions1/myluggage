package myLuggage.services;

import myLuggage.persist.dtos.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EmailService implements IEmailService {

    @Autowired
    private Environment env;

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void confirmRegistration(UserDTO userDTO) {
        String subject = "Registration Confirmation";
        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom(env.getProperty("mail"));
        email.setTo(userDTO.getEmail());
        email.setSubject(subject);
        email.setText("Registration successful\n\n" +
                "This message is an automated reply to your registration request.\n" +
                "To complete registration process you must first confirm your account. To confirm and activate your account please click on the link below.\n" +
                "http://localhost:8080/confirmRegistration?token=" + userDTO.getToken());
        mailSender.send(email);
    }

    @Override
    public void resetPassword(UserDTO userDTO, String newPassword) {
        String subject = "Password reset information";
        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom(env.getProperty("mail"));
        email.setTo(userDTO.getEmail());
        email.setSubject(subject);
        email.setText("Password reset information\n" +
                "This message is an automated reply to your reset password request." +
                "\nYour email: " + userDTO.getEmail()+
                "\nYour username: " + userDTO.getUsername()+
                "\nYour password: " + newPassword +
                "\nRegards, \n My LuggageExcelReportView Service ;)" );
        mailSender.send(email);
    }


}
