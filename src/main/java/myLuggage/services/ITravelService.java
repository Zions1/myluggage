package myLuggage.services;

import myLuggage.persist.dtos.*;
import myLuggage.persist.model.*;

import java.util.List;

/**
 * Service that provides all method used by travel.
 *
 * @author Wiktor Florencki
 */
public interface ITravelService {

    /**
     * Get travel by id.
     * @param id technical identifier of travel
     * @return travel
     */
    TravelDTO getTravelById(int id);

    /**
     * Get travel entity by id.
     * @param id technical identifier of travel
     * @return entity of travel
     */
    TravelEntity getTravelEntityById(Integer id);
    
    /**
     * Gets all travels.
     * @return list of travel
     */
    List<TravelDTO> getAllUserTravels(String email);

    /**
     * Create travel.
     * @param travel travel to create
     */
    Integer addTravel(TravelDTO travel);

    /**
     * Update travel.
     * @param travel travel to update
     */
    void updateTravel(TravelDTO travel);

    /**
     * Delete travel.
     * @param id technical identifier of travel
     */
    void deleteTravel(Integer id);
}
