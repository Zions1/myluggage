package myLuggage.services;

import myLuggage.persist.dtos.CategoryDTO;
import myLuggage.persist.model.CategoryEntity;

import java.util.List;

/**
 * Service that provides all method used by category.
 *
 * @author Wiktor Florencki
 */
public interface ICategoryService {

    /**
     * Get category by id.
     * @param id technical identifier of category
     * @return category
     */
    CategoryDTO getCategoryById(int id);

    /**
     * Get category entity by id.
     * @param id technical identifier of category
     * @return entity of category
     */
    CategoryEntity getCategoryEntityById(int id);

    /**
     * Get category ny name.
     * @param name name of category
     * @return category
     */
    CategoryDTO getCategoryByName(String name);

    /**
     * Get category entity by id.
     * @param name technical identifier of category
     * @return entity of category
     */
    CategoryEntity getCategoryEntityByName(String name);

    /**
     * Get all categories.
     * @return list of categories
     */
    List<CategoryDTO> getAllCategories();

    /**
     * Create category.
     * @param category category
     */
    void addCategory(CategoryDTO category);

    /**
     * Update category.
     * @param category category to update
     */
    void updateCategory(CategoryDTO category);

    /**
     * Delete category.
     * @param id technical identifier of category
     */
    void deleteCategory(int id);
}
