package myLuggage.services;

import myLuggage.persist.daos.interfacesDAO.user.IUserDAO;
import myLuggage.persist.model.UserRoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import myLuggage.persist.daos.implementationsDAO.user.UserDAO;
import myLuggage.persist.model.UserEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service("userDetailsService")
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private IUserDAO userDAO;


    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {

        UserEntity user = userDAO.getUserByUsername(name);
        if (user == null) {
            user = userDAO.getUserByEmail(name);
            if(user == null) {
                throw new UsernameNotFoundException("No user found with username: " + name);
            }
        }

        return new org.springframework.security.core.userdetails.User
                (user.getEmail(), user.getPassword(), user.getActive(),
                        true, true, true, getAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Collection<UserRoleEntity> role) {
        return getGrantedAuthorities(getRoles(role));
    }

    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    private List<String> getRoles(Collection<UserRoleEntity> roles) {
        return roles.stream().map(UserRoleEntity::getRole).collect(Collectors.toList());
    }
}
