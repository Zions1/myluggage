package myLuggage.exception;


/**
 * Represents Repository exceptions.
 *
 * @author Wiktor Florencki
 */
public class RepositoryException extends Exception {

    public RepositoryException(Throwable cause) {
        super(cause);
    }
}
