package myLuggage.exception;

/**
 * @author Wiktor Florencki
 */
public class ActivityException extends Exception {

    public ActivityException(Throwable cause) {
        super(cause);
    }
}
